import React from 'react';
import { createRoot } from 'react-dom/client';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { Provider } from 'react-redux';
import { store } from './app/store';
import reportWebVitals from './reportWebVitals';
import './index.css';
import Mi8 from './pages/Mi8/Mi8';
import LangingPage from './pages/landing-page';

const container = document.getElementById('root');
const root = createRoot(container);

root.render(
  <BrowserRouter>
    <Routes>
      <Route path="/" element={<LangingPage />} />
      <Route path="/helicopters/mi-8/" element={< Mi8 />} />
    </Routes>
  </BrowserRouter>
);

reportWebVitals();
