import React, { Component } from 'react';
import '../Mi8-subpage.scss';
import img_uv__control_panel from './pictures/img_uv__control_panel.png';

const Countermeasures = () => {
    return (<div className='Mi8-subpage'>
        <ul className='fast-instructions'>
            <li>Example Programs:
                <ul>
                    <li><strong>110</strong> - 1 sequence, dispense 1 flare, 0.125s int.</li>
                    <li><strong>622</strong> - 6 sequences, 2 flares, 2s interval</li>
                    <li><strong>529</strong> - 12 sequences, 2 flares, 0.5s interval</li>
                </ul>
            </li>
        </ul>
        <ul className='instructions'>
            <li className='instruction'>Countermeasures Control Panel</li>
            <ol className='sub-instructions'>
                <li className='sub-instruction'>
                    <div className='desc'>Counter Panel - overview</div>
                    <div className='container'>
                        <div className='left'>
                            <div>1) PROGRAM DISPLAY</div>
                            <div>2) DISPENSER SIDE LAMP</div>
                            <div>3) LEFT/RIGHT</div>
                            <div>4) SEQUENCES</div>
                            <div>5) SALVO</div>
                            <div>6) STOP</div>
                            <div>7) DISPERSER SIDE LAMP</div>
                            <div>8) REMAIN PROGRAM</div>
                            <div>9) INTERVAL button</div>
                            <div>10) RESET button</div>
                            <div>11) DISPENSE button</div>
                            <div>Example Programs:</div>
                            <div></div>
                            <div></div>
                            <div></div>
                        </div>
                        <div className='right'>
                            <img src={img_uv__control_panel} alt='' />
                        </div>
                    </div>
                </li>
            </ol>
        </ul>
    </div>);
}

export default Countermeasures;