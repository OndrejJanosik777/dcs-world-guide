import React, { Component } from 'react';
import '../../Mi8-subpage.scss';
import img_center_overhead_console from './pictures/img_center_overhead_console.png';
import img_cockpit_layout from './pictures/img_cockpit_layout.png';
import img_left_circuit_breaker_console from './pictures/img_left_circuit_breaker_console.png';
import img_left_overhead_console from './pictures/img_left_overhead_console.png';
import img_right_circuit_breaker_console from './pictures/img_right_circuit_breaker_console.png';
import img_right_overhead_console from './pictures/img_right_overhead_console.png';

const MiddleOverheadSection = () => {
    return (<div className='Mi8-subpage'>
        <ul className='fast-instructions'>
        </ul>
        <ul className='instructions'>
            <li className='instruction'>COCKPIT LAYOUT</li>
            <ol className='sub-instructions'>
                <li className='sub-instruction'>
                    <div className='desc'>Layout</div>
                    <div className='container_1row'>
                        <div className='full_width'>
                            <img src={img_cockpit_layout} alt='' />
                        </div>
                        <div className='left'>
                            <div></div>
                            <div>1. Left side console</div>
                            <div>2. Left triangular panel</div>
                            <div>3. Intercommunications set SPU-7 control boxes for pilot</div>
                            <div>4. Fuel shutoff levers (fuel cut-off triggers) of the engines</div>
                            <div>5. Pilot Sight PKV</div>
                            <div>6. Left overhead console</div>
                            <div>7. Left circuit breaker console</div>
                            <div>8. Center overhead console</div>
                            <div>9. Right circuit breaker console</div>
                            <div>10. Right overhead console</div>
                            <div> 11. Intercommunications set SPU-7 control boxes for copilot</div>
                            <div>12. Right triangular panel</div>
                            <div>13. Right side console</div>
                            <div>14. Right rear console</div>
                            <div>15. Copilot's weapons control panel</div>
                            <div>16. Outdoor temperature gauge</div>
                            <div>17. Cockpit funs</div>
                            <div>18. Left instrument panel</div>
                            <div>19. Right instrument panel</div>
                            <div>20. ЭСБР-3П/А (ESBR-3P/A) Electrical release control box</div>
                            <div>21. Center console</div>
                            <div>22. Copilot Sight ОПБ-1р (OPB-1R) – bombing sight, not modeled</div>
                            <div>23. Right auxiliary panel</div>
                            <div>24. Rotor brake lever</div>
                            <div>25. Throttle handles</div>
                            <div>26. Collective control handle</div>
                            <div>27. Anti-torque pedals</div>
                            <div>28. Pitot tube selector</div>
                            <div>29. Cyclic control stick</div>
                            <div>30. G-load indicator</div>
                            <div>31. Magnetic compas КИ-13 (KI-13)</div>
                        </div>
                    </div>
                </li>
            </ol>
            <li className='instruction'>MIDDLE OVERHEAD SECTION</li>
            <ol className='sub-instructions'>
                <li className='sub-instruction'>
                    <div className='desc'>LEFT OVERHEAD CONSOLE</div>
                    <div className='container_1row'>
                        <div className='full_width'>
                            <img src={img_left_overhead_console} alt='' />
                        </div>
                        <div className='left'>
                            <div>1. Pilot’s weapons control panel</div>
                            <div>2. Anti-ice system control panel</div>
                            <div>3. “ОБОГРЕВ ИСПРАВЕН” (Anti-ice normal) annunciator</div>
                            <div>4. Р-863 (R-863) VHF radio FM/AM switch</div>
                            <div>5. Р-863 (R-863) VHF radio channel selector</div>
                            <div>6. Anti-ice system annunciator panel</div>
                            <div>7. АФ1-150 (AF1-150) ammeter</div>
                            <div>8. Section 1…4 annunciator panel</div>
                            <div>9. Ammeter load current selector switch</div>
                            <div>10. “ОБЛЕДЕН” (Icing) “ПОС ВКЛЮЧЕНА” (Anti-iceON) annunciators</div>
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>CENTER OVERHEAD CONSOLE</div>
                    <div className='container_1row'>
                        <div className='full_width'>
                            <img src={img_center_overhead_console} alt='' />
                        </div>
                        <div className='left'>
                            <div>1. Fire protection system panel</div>
                            <div>2. Fire protection system panel annunciators</div>
                            <div>3. APU start control panel</div>
                            <div>4. Fire protection system test panel</div>
                            <div>5. APU EGT indicator</div>
                            <div>6. APU air pressure indicator</div>
                            <div>7. Fuel system control panel</div>
                            <div>8. Engine start control panel</div>
                            <div>9. Hydraulic system control panel</div>
                            <div>10. Main hydraulic system pressure indicator</div>
                            <div>11. Reserve hydraulic system pressure indicator</div>
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>RIGHT OVERHEAD CONSOLE</div>
                    <div className='container_1row'>
                        <div className='full_width'>
                            <img src={img_right_overhead_console} alt='' />
                        </div>
                        <div className='left'>
                            <div>1. АРК-15 (ARK-15) ADF control panel</div>
                            <div>2. АРК-УД (ARK-UD) ADF control panel</div>
                            <div>3. ПУ-26 (PU-26) control panel of the ГМК-1А (GMK-1A) gyrocompass system</div>
                            <div>4. АРК-15 (ARK-15) frequency selector</div>
                            <div>5. КО-50 (KO-50) heater temp regulator switch</div>
                            <div>6. КО-50 (KO-50) heater control panel with annunciators</div>
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>LEFT CIRCUIT BREAKER CONSOLE</div>
                    <div className='container_1row'>
                        <div className='full_width'>
                            <img src={img_left_circuit_breaker_console} alt='' />
                        </div>
                        <div className='left'>
                            <div>1. Aiming correction table</div>
                            <div>2. Weapons arming panel</div>
                            <div>3. Weapon systems circuit breakers</div>
                            <div>4. Remaining ammunition indicators</div>
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>RIGHT CIRCUIT BREAKER CONSOLE</div>
                    <div className='container_1row'>
                        <div className='full_width'>
                            <img src={img_right_circuit_breaker_console} alt='' />
                        </div>
                        <div className='left'>
                            --
                        </div>
                    </div>
                </li>
            </ol>
        </ul>
    </div>);
}

export default MiddleOverheadSection;