import React, { Component } from 'react';
import '../../Mi8-subpage.scss';
import img_cockpit_layout from './pictures/img_cockpit_layout.png';
import img_right_auxiliary_panel from './pictures/img_right_auxiliary_panel.png';
import img_right_rear_console from './pictures/img_right_rear_console.png';

const AdditionalPanels = () => {
    return (<div className='Mi8-subpage'>
        <ul className='fast-instructions'>
        </ul>
        <ul className='instructions'>
            <li className='instruction'>COCKPIT LAYOUT</li>
            <ol className='sub-instructions'>
                <li className='sub-instruction'>
                    <div className='desc'>Layout</div>
                    <div className='container_1row'>
                        <div className='full_width'>
                            <img src={img_cockpit_layout} alt='' />
                        </div>
                        <div className='left'>
                            <div></div>
                            <div>1. Left side console</div>
                            <div>2. Left triangular panel</div>
                            <div>3. Intercommunications set SPU-7 control boxes for pilot</div>
                            <div>4. Fuel shutoff levers (fuel cut-off triggers) of the engines</div>
                            <div>5. Pilot Sight PKV</div>
                            <div>6. Left overhead console</div>
                            <div>7. Left circuit breaker console</div>
                            <div>8. Center overhead console</div>
                            <div>9. Right circuit breaker console</div>
                            <div>10. Right overhead console</div>
                            <div> 11. Intercommunications set SPU-7 control boxes for copilot</div>
                            <div>12. Right triangular panel</div>
                            <div>13. Right side console</div>
                            <div>14. Right rear console</div>
                            <div>15. Copilot's weapons control panel</div>
                            <div>16. Outdoor temperature gauge</div>
                            <div>17. Cockpit funs</div>
                            <div>18. Left instrument panel</div>
                            <div>19. Right instrument panel</div>
                            <div>20. ЭСБР-3П/А (ESBR-3P/A) Electrical release control box</div>
                            <div>21. Center console</div>
                            <div>22. Copilot Sight ОПБ-1р (OPB-1R) – bombing sight, not modeled</div>
                            <div>23. Right auxiliary panel</div>
                            <div>24. Rotor brake lever</div>
                            <div>25. Throttle handles</div>
                            <div>26. Collective control handle</div>
                            <div>27. Anti-torque pedals</div>
                            <div>28. Pitot tube selector</div>
                            <div>29. Cyclic control stick</div>
                            <div>30. G-load indicator</div>
                            <div>31. Magnetic compas КИ-13 (KI-13)</div>
                        </div>
                    </div>
                </li>
            </ol>
            <li className='instruction'>ADDITIONAL PANELS</li>
            <ol className='sub-instructions'>
                <li className='sub-instruction'>
                    <div className='desc'>RIGHT REAR CONSOLE</div>
                    <div className='container_1row'>
                        <div className='full_width'>
                            <img src={img_right_rear_console} alt='' />
                        </div>
                        <div className='left'>
                            <div>1. DC voltmeter</div>
                            <div>2. DC battery 1 ammeter</div>
                            <div>3. DC battery 2 ammeter</div>
                            <div>4. AC rectifier 1 voltmeter</div>
                            <div>5. AC rectifier 2 voltmeter</div>
                            <div>6. AC rectifier 3 voltmeter</div>
                            <div>7. AC generator voltmeter</div>
                            <div>8. AC generator 1 ammeter</div>
                            <div>9. AC generator 2 ammeter</div>
                            <div>10. AC power control panel</div>
                            <div>11. AC voltage control rotary 1/2</div>
                            <div>12. Inverter 1 MAN/AUTO switch</div>
                            <div>13. Inverter 2 MAN/AUTO switch</div>
                            <div>14. External power switch</div>
                            <div>15. Generator 1, 2 fail; External power, PO-500 heater annunciators</div>
                            <div>16. Generator 2 switch</div>
                            <div>17. Generator 1 switch</div>
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>RIGHT AUXILIARY PANEL</div>
                    <div className='container_1row'>
                        <div className='full_width'>
                            <img src={img_right_auxiliary_panel} alt='' />
                        </div>
                        <div className='left'>
                            <div>1. Р-828 (R-828) radio control panel</div>
                            <div>2. Р-828 (R-828) radio power switch</div>
                            <div>3. Р-828 (R-828) ANT-ADF switch</div>
                            <div>4. УВ-26 (UV-26) countermeasures control panel</div>
                            <div>5. Yadro-1I HF radio set control panel</div>
                        </div>
                    </div>
                </li>
            </ol>
        </ul>
    </div>);
}

export default AdditionalPanels;