import React, { Component } from 'react';
import '../../Mi8-subpage.scss';
import img_cockpit_layout from './pictures/img_cockpit_layout.png';
import img_center_console from './pictures/img_center_console.png';
import img_left_instrumental_panel from './pictures/img_left_instrumental_panel.png';
import img_right_instrumental_panel from './pictures/img_right_instrumental_panel.png';
import img_warning_light_left from './pictures/img_warning_light_left.png';
import img_warning_light_middle from './pictures/img_warning_light_middle.png';
import img_warning_light_right from './pictures/img_warning_light_right.png';

const FrontCentral = () => {
    return (<div className='Mi8-subpage'>
        <ul className='fast-instructions'>
        </ul>
        <ul className='instructions'>
            <li className='instruction'>COCKPIT LAYOUT</li>
            <ol className='sub-instructions'>
                <li className='sub-instruction'>
                    <div className='desc'>Layout</div>
                    <div className='container_1row'>
                        <div className='full_width'>
                            <img src={img_cockpit_layout} alt='' />
                        </div>
                        <div className='left'>
                            <div></div>
                            <div>1. Left side console</div>
                            <div>2. Left triangular panel</div>
                            <div>3. Intercommunications set SPU-7 control boxes for pilot</div>
                            <div>4. Fuel shutoff levers (fuel cut-off triggers) of the engines</div>
                            <div>5. Pilot Sight PKV</div>
                            <div>6. Left overhead console</div>
                            <div>7. Left circuit breaker console</div>
                            <div>8. Center overhead console</div>
                            <div>9. Right circuit breaker console</div>
                            <div>10. Right overhead console</div>
                            <div> 11. Intercommunications set SPU-7 control boxes for copilot</div>
                            <div>12. Right triangular panel</div>
                            <div>13. Right side console</div>
                            <div>14. Right rear console</div>
                            <div>15. Copilot's weapons control panel</div>
                            <div>16. Outdoor temperature gauge</div>
                            <div>17. Cockpit funs</div>
                            <div>18. Left instrument panel</div>
                            <div>19. Right instrument panel</div>
                            <div>20. ЭСБР-3П/А (ESBR-3P/A) Electrical release control box</div>
                            <div>21. Center console</div>
                            <div>22. Copilot Sight ОПБ-1р (OPB-1R) – bombing sight, not modeled</div>
                            <div>23. Right auxiliary panel</div>
                            <div>24. Rotor brake lever</div>
                            <div>25. Throttle handles</div>
                            <div>26. Collective control handle</div>
                            <div>27. Anti-torque pedals</div>
                            <div>28. Pitot tube selector</div>
                            <div>29. Cyclic control stick</div>
                            <div>30. G-load indicator</div>
                            <div>31. Magnetic compas КИ-13 (KI-13)</div>
                        </div>
                    </div>
                </li>
            </ol>
            <li className='instruction'>FRONT CENTRAL SECTIONS</li>
            <ol className='sub-instructions'>
                <li className='sub-instruction'>
                    <div className='desc'>LEFT INSTRUMENTAL PANEL</div>
                    <div className='container_1row'>
                        <div className='full_width'>
                            <img src={img_left_instrumental_panel} alt='' />
                        </div>
                        <div className='left'>
                            <div>1. Pilot's landing/search and taxi light controls</div>
                            <div>2. УР-117М (UR-117M) engine pressure radio (EPR) indicator</div>
                            <div>3. ИП-21 (IP-21) main rotor pitch angle indicator</div>
                            <div>4. ИТЭ-2Т (ITE-2T) two-pointer engine tachometer indicator</div>
                            <div>5. ИТЭ-1Т (ITE-1T) main rotor tachometer indicator</div>
                            <div>6. Radar altimeter switch</div>
                            <div>7. УС-450К (US-450K) airspeed indicator</div>
                            <div>8. УВ-5M (UV-5M) radar altimeter indicator</div>
                            <div>9. ВД-10ВК (VD-10VK) pressure altimeter indicator</div>
                            <div>10. ОПБ-1Р (OPB-1R) bomb sight course indicator</div>
                            <div>11. АРК СВ-УКВ (ADF HF-VHF) switch</div>
                            <div>12. УГР-4УК (UGR-4UK) directional gyro</div>
                            <div>13. АГБ-3К (AGB-3K) attitude indicator</div>
                            <div>14. Hover and low speed control indicator</div>
                            <div>15. ВР-30МК (VR-30MK) vertical velocity indicator</div>
                            <div>16. Manual flare dispersion button at UV-26 countermeasures</div>
                            <div>17. Annunciators (lights)</div>
                            <div>18. ЭУП-53 (EUP-53) turn indicator</div>
                            <div>19. "СЕТЬ ПИТ.ОТ АКК" ("BATTARY IN USE") light (above) and “ОТАКАЗ 6201” (“6201 FAIL”) (below)</div>
                            <div>20. Annunciators (lights)</div>
                            <div>21. 2УТ-6К (2UT-6K) exhaust gas temperature indicator</div>
                            <div>22. Annunciators (lights)</div>
                            <div>23. Pitot tube selector</div>
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>WARNING LIGHT RIGHT</div>
                    <div className='container'>
                        <div className='left'>
                            <div>1. Сhips in Main Gearbox</div>
                            <div>2. Сhips in Intermediate Gearbox</div>
                            <div>3. Сhips in Tail Rotor Gearbox</div>
                        </div>
                        <div className='right'>
                            <img src={img_warning_light_right} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>WARNING LIGHT MIDDLE</div>
                    <div className='container'>
                        <div className='left'>
                            <div>1. Left (right) engine Free Turbin Overspeeding</div>
                            <div>2. Left (right) engine Oil Pressure is Low</div>
                            <div>3. Electronic Control left (right) engine OFF</div>
                            <div>4. Emergency Power (ЧР – Чрезвычайный Режим) left (right) engine</div>
                        </div>
                        <div className='right'>
                            <img src={img_warning_light_middle} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>WARNING LIGHT LEFT</div>
                    <div className='container'>
                        <div className='left'>
                            <div>1. Сhips in left (right) engine Oil</div>
                            <div>2. Fuel Filter Clogging left (right) engine</div>
                            <div>3. Left (right) engine Abnormal vibration</div>
                            <div>4. Left (right) engine Excursion Limit vibration</div>
                            <div> 5. Light is not used</div>
                            <div>6. Fire</div>
                        </div>
                        <div className='right'>
                            <img src={img_warning_light_left} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>RIGHT INSTRUMENTAL PANEL</div>
                    <div className='container_1row'>
                        <div className='full_width'>
                            <img src={img_right_instrumental_panel} alt='' />
                        </div>
                        <div className='left'>
                            <div>1. УС-450К (US-450K) airspeed indicator</div>
                            <div>2. ВД-10ВК (VD-10VK) pressure altimeter indicator</div>
                            <div>3. АГБ-3К (AGB-3K) attitude indicator</div>
                            <div>4. УГР-4УК (UGR-4UK) directional gyro</div>
                            <div>5. ВР-30МК (VR-30MK) vertical velocity indicator</div>
                            <div> 6. “ДИСС ОТКАЗАЛ” Doppler system fail annunciator</div>
                            <div>7. ИТЭ-1Т (ITE-1T) main rotor tachometer indicator</div>
                            <div>8. ИТЭ-2Т (ITE-2T) two-pointer engine tachometer indicator</div>
                            <div>9. Copilot's landing/search light switch</div>
                            <div>10. ТВ-1 (TV-1) cabin temperature indicator</div>
                            <div>11. ДИСС-15 (DISS-15) Doppler system coordinate indicator</div>
                            <div>12. ДИСС-15 (DISS-15) Doppler system ground speed and drift indicator</div>
                            <div> 13. БЭ-09К (BE-09K) fuel quantity indicator</div>
                            <div>14. Low Fuel (270 L) annunciator</div>
                            <div>15. П-8УК (P-8UK) fuel meter switch АЧС-1 (AChS-1) clock</div>
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>CENTER CONSOLE</div>
                    <div className='container_1row'>
                        <div className='full_width'>
                            <img src={img_center_console} alt='' />
                        </div>
                        <div className='left'>
                            <div>1. УИЗ-6 (UIZ-6) main transmission oil temp, intermediate and tail rotor gearbox oil pressure indicator</div>
                            <div>2. ТУЭ-48 (TUE-48) main transmission oil temp indicator</div>
                            <div>3. УИЗ-3 (UIZ-3) left engine oil pressure and temp indicator</div>
                            <div>4. УИЗ-3 (UIZ-3) right engine oil pressure and temp indicator</div>
                            <div>5. Р-863 (R-863) VHF radio manual/preset selector</div>
                            <div>6. Р-863 (R-863) VHF radio control panel</div>
                            <div>7. Р-863 (R-863) VHF radio frequency select panel</div>
                            <div>8. Engine governor control panel</div>
                            <div>9. Lamp test and electrical system backup switches</div>
                            <div>10. АП-34Б (AP-34B) autopilot control panel</div>
                            <div>11. БУ-32-1 (BU-32-1) control unit for the СПУУ-52 (SPUU-52) pitch limit system</div>
                            <div>12. ИН-4 (IN-4) trim indicator of the automatic flight control system</div>
                        </div>
                    </div>
                </li>
            </ol>
        </ul>
    </div>);
}

export default FrontCentral;