import React, { Component } from 'react';
import '../../Mi8-subpage.scss';
import img_left_side_console from './pictures/img_left_side_console.png';
import img_cockpit_layout from './pictures/img_cockpit_layout.png';
import img_left_triangular_panel from './pictures/img_left_triangular_panel.png';
import img_right_side_console from './pictures/img_right_side_console.png';
import img_right_triangular_panel from './pictures/img_right_triangular_panel.png';

const SideOverheadSection = () => {
    return (<div className='Mi8-subpage'>
        <ul className='fast-instructions'>
        </ul>
        <ul className='instructions'>
            <li className='instruction'>COCKPIT LAYOUT</li>
            <ol className='sub-instructions'>
                <li className='sub-instruction'>
                    <div className='desc'>Layout</div>
                    <div className='container_1row'>
                        <div className='full_width'>
                            <img src={img_cockpit_layout} alt='' />
                        </div>
                        <div className='left'>
                            <div></div>
                            <div>1. Left side console</div>
                            <div>2. Left triangular panel</div>
                            <div>3. Intercommunications set SPU-7 control boxes for pilot</div>
                            <div>4. Fuel shutoff levers (fuel cut-off triggers) of the engines</div>
                            <div>5. Pilot Sight PKV</div>
                            <div>6. Left overhead console</div>
                            <div>7. Left circuit breaker console</div>
                            <div>8. Center overhead console</div>
                            <div>9. Right circuit breaker console</div>
                            <div>10. Right overhead console</div>
                            <div> 11. Intercommunications set SPU-7 control boxes for copilot</div>
                            <div>12. Right triangular panel</div>
                            <div>13. Right side console</div>
                            <div>14. Right rear console</div>
                            <div>15. Copilot's weapons control panel</div>
                            <div>16. Outdoor temperature gauge</div>
                            <div>17. Cockpit funs</div>
                            <div>18. Left instrument panel</div>
                            <div>19. Right instrument panel</div>
                            <div>20. ЭСБР-3П/А (ESBR-3P/A) Electrical release control box</div>
                            <div>21. Center console</div>
                            <div>22. Copilot Sight ОПБ-1р (OPB-1R) – bombing sight, not modeled</div>
                            <div>23. Right auxiliary panel</div>
                            <div>24. Rotor brake lever</div>
                            <div>25. Throttle handles</div>
                            <div>26. Collective control handle</div>
                            <div>27. Anti-torque pedals</div>
                            <div>28. Pitot tube selector</div>
                            <div>29. Cyclic control stick</div>
                            <div>30. G-load indicator</div>
                            <div>31. Magnetic compas КИ-13 (KI-13)</div>
                        </div>
                    </div>
                </li>
            </ol>
            <li className='instruction'>SIDE OVERHEAD SECTIONS</li>
            <ol className='sub-instructions'>
                <li className='sub-instruction'>
                    <div className='desc'>LEFT SIDE CONSOLE</div>
                    <div className='container_1row'>
                        <div className='full_width'>
                            <img src={img_left_side_console} alt='' />
                        </div>
                        <div className='left'>
                            <div>1. Left side group 1/2 red lighting dimmers</div>
                            <div>2. “РТ ЛЕВ РАБОТАЕТ” “РТ ПРАВ РАБОТАЕТ” LH/RH engine temp regulator operating annunciators</div>
                            <div>3. 7П-662 (7P-662) signal flares control panel</div>
                            <div>4. “САРПП РАБОТАЕТ” flight data recorder (FDR) operating annunciator</div>
                            <div>5. МВУ-10К (MVU-10K) pneumatic system air pressure gauge</div>
                            <div>6. РИ-65Б (RI-65B) voice warning system remote control panel</div>
                            <div>7. МА-60К (MA-60K) air pressure gauge for the landing gear wheel brake system</div>
                            <div>8. Control panel 484 of “device 6201” (IFF responder)</div>
                            <div>9. Control panel 485 of “device 6201” (IFF responder</div>
                            <div>10. П-503Б (P-503B) cockpit voice recorder (CVR) control panel</div>
                            <div>11. “ВКЛЮЧИ ЗАПАСНОЙ” (“Set Reserve”) annunciator</div>
                            <div>12. External cargo auto release switch</div>
                            <div>13. “СТВОРКИ ОТКРЫТЫ” (“Doors open”) annunciator</div>
                            <div>14. “ЗАМОК ОТКРЫТ” (“Shackle open”) annunciator</div>
                            <div>15. “СИРЕНА ВКЛЮЧЕНА” (“Horn on”) annunciator</div>
                            <div>16. Air horn button</div>
                            <div>17. Code NAV lights button</div>
                            <div>18. FDR power switch</div>
                            <div>19. LH/RH engine temp regulator test buttons</div>
                            <div>20. EGT gauge ground and air test buttons</div>
                            <div>21. ИВ-500Е (IV-500E) engine vibration indicator test button</div>

                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>LEFT TRIANGULAR PANEL</div>
                    <div className='container_1row'>
                        <div className='full_width'>
                            <img src={img_left_triangular_panel} alt='' />
                        </div>
                        <div className='left'>
                            <div>1. Windshield wiper switch</div>
                            <div>2. СПУУ-52 (SPUU-52) tail rotor pitch limit system power switch</div>
                            <div>3. Radar altimeter power switch</div>
                            <div>4. РИ-65 (RI-65) voice warning system (VWS) power switch</div>
                            <div>5. Pitot tube heating test switch</div>
                            <div>6. “ОБОГРЕВ ИСПРАВЕН” (Heater OK) annunciator</div>

                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>RIGHT SIDE CONSOLE</div>
                    <div className='container_1row'>
                        <div className='full_width'>
                            <img src={img_right_side_console} alt='' />
                        </div>
                        <div className='left'>
                            <div>1. Right side group 1/2 red lighting dimmers</div>
                            <div>2. “ЛЕВ/ПРАВ ПЗУ ВКЛЮЧЕН”(L/R Dust Protection ON) annunciators</div>
                            <div>3. APU generator load indicator</div>
                            <div>4. DC power control panel</div>
                            <div>5. Annunciators brightness switch</div>
                            <div>6. Warning blinker switch</div>
                            <div>7. Rectifiers, external power, and BIT annunciators</div>
                            <div>8. LH/RH pitot tube, clock, and battery heating switches</div>
                            <div>9. L/R engine dust protection switches</div>
                            <div>10. Strobe light switch</div>
                            <div>11. Rotor tip and formation light switches</div>
                            <div>12. Navigation and formation lights brightness switches</div>
                            <div>13. General and standby cabin lighting switches</div>
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>RIGHT TRIANGULAR PANEL</div>
                    <div className='container_1row'>
                        <div className='full_width'>
                            <img src={img_right_triangular_panel} alt='' />
                        </div>
                        <div className='left'>
                            <div>1. Windshield wiper switch</div>
                            <div>2. ДИСС-15 (DISS-15) Doppler system and ЯДРО-1А (Yadro-1A) radio control panel lighting switch</div>
                            <div>3. Microphone power switch</div>
                            <div>4. VHF-ADF interlock switch</div>
                            <div>5. Dome light switch</div>
                            <div>6. Fan power switch</div>
                            <div>7. Right attitude indicator power switch</div>
                            <div>8. Astrocompass power switch</div>
                            <div>9. ГМК-1 (GMK-1) gyrocompass system power switch</div>
                            <div>10. Yadro-1A HF radio power switch</div>
                            <div>11. Doppler system power switch</div>
                            <div>12. “ОБОГРЕВ ИСПРАВЕН” (Heater OK) annunciator</div>
                            <div>13. Pitot tube heating test switch</div>
                        </div>
                    </div>
                </li>
            </ol>
        </ul>
    </div>);
}

export default SideOverheadSection;