import React, { Component } from 'react';
import '../Mi8-subpage.scss';
import img_ark_ud_control_panel_overview from './pictures/img_ark_ud_control_panel_overview.png';
import img_ark_ud_control_panel from './pictures/img_ark_ud_control_panel.png';
import img_source_selector_ark_uk from './pictures/img_source_selector_ark_uk.png';
import img_ugr_4uk_needle_right from './pictures/img_ugr_4uk_needle_right.png';

const Ark_ud = () => {
    return (<div className='Mi8-subpage'>
        <ul className='fast-instructions'>
            <li>Using ARK-UD Radio Navigation
                <ol>
                    <li>Turn on circuit breakers</li>
                    <li>Source Selector - PK2</li>
                    <li>ICS RADIO Selector - DOWN</li>
                    <li>Set Switch UGR-4UK - RIGHT</li>
                    <li>ARK UD Control Panel</li>
                </ol>
            </li>
        </ul>
        <div>Usage: search & rescue missions</div>
        <ul className='instructions'>
            <li className='instruction'>Using ARK-UD Radio Navigation</li>
            <ol className='sub-instructions'>
                <li className='sub-instruction'>
                    <div className='desc'>Circuit breakers - Up</div>
                    <div className='container'>
                        <div className='left'>
                            <strong>--</strong>
                        </div>
                        <div className='right'>
                            <strong>--</strong>
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Source Selector - PK2</div>
                    <div className='container'>
                        <div className='left'>
                            <div>a) SET Source Selector to PK2</div>
                            <div>b) ICS-RADIO Selector to DOWN</div>
                        </div>
                        <div className='right'>
                            <img src={img_source_selector_ark_uk} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Switch UGR-4UK</div>
                    <div className='container'>
                        <div className='left'>
                            <div>a) Set Switch UGR-4UK - RIGHT</div>
                        </div>
                        <div className='right'>
                            <img src={img_ugr_4uk_needle_right} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>ARK-UD Control Panel</div>
                    <div className='container'>
                        <div className='left'>
                            <div>a) mode selector - NARROW BAND (2nd); when the Wide Band lamp iluminates as the helicoper nears beacon; switch to WIDE BAND (3rd)</div>
                            <div>b) FREQUENCY BAND SWITCH to corresponding required band; </div>
                        </div>
                        <div className='right'>
                            <img src={img_ark_ud_control_panel} alt='' />
                        </div>
                    </div>
                </li>
            </ol>
            <li className='instruction'>ARK-UD Control Panel overview</li>
            <ol className='sub-instructions'>
                <li className='sub-instruction'>
                    <div className='desc'>ARK-UD Control Panel</div>
                    <div className='container'>
                        <div className='left'>
                            <div>1) OFF - NARROW BAND - WIDE BAND - PULSE - RPK</div>
                            <div>2) Sensitivity HIGH - LOW</div>
                            <div>3) CHANNEL selector</div>
                            <div>4) ANTENNA L/R - rotate manually antenna</div>
                            <div>5) TEST BUTTON- self-test mode operation</div>
                            <div>6) VOLUME CONTROL KNOB</div>
                            <div>7) FREQUENCY BAND SWITCH: VHF - UP; UHF - DOWN</div>
                        </div>
                        <div className='right'>
                            <img src={img_ark_ud_control_panel_overview} alt='' />
                        </div>
                    </div>
                </li>
            </ol>
        </ul>
    </div>);
}

export default Ark_ud;