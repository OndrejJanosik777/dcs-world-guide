import React, { Component } from 'react';
import '../Mi8-subpage.scss';
import img_activate_migalka from './pictures/img_activate_migalka.png';
import img_backup_dome_lights from './pictures/img_backup_dome_lights.png';
import img_enable_red_backlighting from './pictures/img_enable_red_backlighting.png';
import img_migalka_day_night from './pictures/img_migalka_day_night.png';
import img_pilot_flashlight from './pictures/img_pilot_flashlight.png';
import img_red_backlight_group1 from './pictures/img_red_backlight_group1.png';
import img_red_backlight_group2 from './pictures/img_red_backlight_group2.png';
import img_red_backlight_group3 from './pictures/img_red_backlight_group3.png';

const InteriorLights = () => {
    return (<div className='Mi8-subpage'>
        <ul className='fast-instructions'>
            <li>Internal lights - overview
                <ol>
                    <li>Flashlight</li>
                    <li>Backup dome lights - left, right</li>
                    <li>Red Backlighting - Group 1, 2, 3</li>
                    <li>Migalka - Night</li>
                </ol>
            </li>
        </ul>
        <ul className='instructions'>
            <li className='instruction'>Interior lights</li>
            <ol className='sub-instructions'>
                <li className='sub-instruction'>
                    <div className='desc'>Using Pilot Flashlight</div>
                    <div className='container'>
                        <div className='left'>
                            <strong>Lalt+L</strong>
                        </div>
                        <div className='right'>
                            <img src={img_pilot_flashlight} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Backup dome lights</div>
                    <div className='container'>
                        <div className='left'>
                            turning on backup dome lights on left and right side
                        </div>
                        <div className='right'>
                            <img src={img_backup_dome_lights} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Enable Red Backlighting</div>
                    <div className='container'>
                        <div className='left'>
                            enable red backlight
                        </div>
                        <div className='right'>
                            <img src={img_enable_red_backlighting} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Red Backglight Group 1</div>
                    <div className='container'>
                        <div className='left'>
                            red backlight
                        </div>
                        <div className='right'>
                            <img src={img_red_backlight_group1} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Red Backglight Group 2</div>
                    <div className='container'>
                        <div className='left'>
                            red backlight
                        </div>
                        <div className='right'>
                            <img src={img_red_backlight_group2} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Red Backglight Group 3</div>
                    <div className='container'>
                        <div className='left'>
                            red backlight
                        </div>
                        <div className='right'>
                            <img src={img_red_backlight_group3} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Activate Migalka - warning system</div>
                    <div className='container'>
                        <div className='left'>
                            up - active
                        </div>
                        <div className='right'>
                            <img src={img_activate_migalka} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Set Migalka for Night</div>
                    <div className='container'>
                        <div className='left'>
                            <div>UP - Night</div>
                            <div>DOWN - Day</div>
                        </div>
                        <div className='right'>
                            <img src={img_migalka_day_night} alt='' />
                        </div>
                    </div>
                </li>
            </ol>
        </ul>
    </div>);
}

export default InteriorLights;