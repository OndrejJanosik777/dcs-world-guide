import React, { Component } from 'react';
import '../Mi8-subpage.scss';
import img_copilots_weapon_control_panel_3 from './pictures/img_copilots_weapon_control_panel_3.png';
import img_pilots_upper_arm_panel_3 from './pictures/img_pilots_upper_arm_panel_3.png';
import img_electrical_release_box_panel from './pictures/img_electrical_release_box_panel.png';
import img_master_arm_switch from './pictures/img_master_arm_switch.png';
import img_weapons_cb from './pictures/img_weapons_cb.png';

const Bombs = () => {
    return (<div className='Mi8-subpage'>
        <ul className='fast-instructions'>
            <li>Engaging B8v20 rockets - overview
                <ol>
                    <li>Circuit breakers</li>
                    <li>Master Arm Switch</li>
                    <li>Pilot's upper arm panel</li>
                    <li>Pilot's lower arm panel</li>
                    <li>Gun Camera - optional</li>
                    <li>Copilot's weapon control panel</li>
                    <li>Fire</li>
                </ol>
            </li>
        </ul>
        <ul className='instructions'>
            <li className='instruction'>Deploying BOMBS</li>
            <ol className='sub-instructions'>
                <li className='sub-instruction'>
                    <div className='desc'>Circuit Brakers - UP</div>
                    <div className='container'>
                        <div className='left'>
                            <strong>--</strong>
                        </div>
                        <div className='right'>
                            <img src={img_weapons_cb} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Enable master arm switch</div>
                    <div className='container'>
                        <div className='left'>
                            <strong>--</strong>
                        </div>
                        <div className='right'>
                            <img src={img_master_arm_switch} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Pilot's Upper Armament Panel</div>
                    <div className='container'>
                        <div className='left'>
                            <div>a) PRESS check lamps button</div>
                            <div>b) CHECK LIGHTS - that all lamps are on</div>
                            <div>c) ENABLE RKT Gun maser switch</div>
                            <div>d) CHECK lights are on, hardpoints 1-6 are loaded</div>
                        </div>
                        <div className='right'>
                            <img src={img_pilots_upper_arm_panel_3} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Copilot's Weapons Control Panel</div>
                    <div className='container'>
                        <div className='left'>
                            <div>a) On Copilots weapons control panel set PAYLOAD PROFILE to the position correspondin to the current payload profile with bombs</div>
                            <div>b) SWITCH UP - enable master arms bombs</div>
                        </div>
                        <div className='right'>
                            <img src={img_copilots_weapon_control_panel_3} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Electrical Release Box Panel</div>
                    <div className='container'>
                        <div className='left'>
                            <div>a) SET IMPULSE GENERATING KNOB to one of the two positions; I- single bomb, II-pair release</div>
                            <div>b) ENABLE the ESBR to the right</div>
                            <div>c) Release bombs by pressing B</div>
                        </div>
                        <div className='right'>
                            <img src={img_electrical_release_box_panel} alt='' />
                        </div>
                    </div>
                </li>
            </ol>

        </ul>
    </div>);
}

export default Bombs;