import React, { Component } from 'react';
import '../Mi8-subpage.scss';
import img_fire_button from './pictures/img_fire_button.png';
import img_gun_camera from './pictures/img_gun_camera.png';
import img_master_arm_switch from './pictures/img_master_arm_switch.png';
import img_weapons_cb from './pictures/img_weapons_cb.png';
import img_copilots_weapon_control_panel_2 from './pictures/img_copilots_weapon_control_panel_2.png';
import img_pilots_lower_arm_panel_3 from './pictures/img_pilots_lower_arm_panel_3.png';
import img_pilots_upper_arm_panel_2 from './pictures/img_pilots_upper_arm_panel_2.png';
import img_four_position_switch from './pictures/img_four_position_switch.png';

const MG_12_7_GL_30 = () => {
    return (<div className='Mi8-subpage'>
        <ul className='fast-instructions'>
            <li>Deploying MG & GL - overview
                <ol>
                    <li>Circuit breakers - ON</li>
                    <li>Master Arm Switch - ON</li>
                    <li>Copilot's weapon control panel</li>
                    <li>Pilot's upper arm panel</li>
                    <li>Pilot's lower arm panel</li>
                    <li>Four Position Switch</li>
                    <li>Gun Camera - optional</li>
                    <li>Fire</li>
                </ol>
            </li>
        </ul>
        <ul className='instructions'>
            <li className='instruction'>Deploying MG and GL</li>
            <ol className='sub-instructions'>
                <li className='sub-instruction'>
                    <div className='desc'>Circuit Brakers - UP</div>
                    <div className='container'>
                        <div className='left'>
                            <strong>--</strong>
                        </div>
                        <div className='right'>
                            <img src={img_weapons_cb} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Enable master arm switch</div>
                    <div className='container'>
                        <div className='left'>
                            <strong>--</strong>
                        </div>
                        <div className='right'>
                            <img src={img_master_arm_switch} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Copilot's Weapon Control Panel</div>
                    <div className='container'>
                        <div className='left'>
                            <div>On Copilots weapons control panelset PAYLOAD PROFILE to GUV position</div>
                            <div></div>
                            <div>In this position firing and signalization circuits of the GUV containers are connected (rockets, UPK‘s and bombs circuits are disconnected)</div>
                        </div>
                        <div className='right'>
                            <img src={img_copilots_weapon_control_panel_2} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Pilot's Upper Armament Panel</div>
                    <div className='container'>
                        <div className='left'>
                            <div>a) PRESS check lamps button</div>
                            <div>b) CHECK LIGHTS - that all lamps are on</div>
                            <div>c) ENABLE RKT Gun maser switch and check that hardpoints 2, 5 are loaded</div>
                            <div>d) CHECK lights are on, hardpoints 2, 5 are loaded</div>
                            <div>e) CHECK lights are on, rockets launching circuit</div>
                        </div>
                        <div className='right'>
                            <img src={img_pilots_upper_arm_panel_2} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Pilot's Lower Armament Panel</div>
                    <div className='container'>
                        <div className='left'>
                            <div>a) Set Burst length limilation - NO LIMITATION - DOWN</div>
                            <div>b) Variant 800-624/622+800 switch to 800 UP position (if GUV granade launchers AP-30 are on hardpoints 1,2,5,6)</div>
                            <div>c) Variant 800-624/622+800 switch to 624/622+800 DOWN position (if GUV granade launchers AP-30 are on hardpoints 1,6) and maschine guns on hardpoints 2,5;</div>
                        </div>
                        <div className='right'>
                            <img src={img_pilots_lower_arm_panel_3} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>
                        Copilot's Weapons Control Panel
                    </div>
                    <div className='container'>
                        <div className='left'>
                            On co-pilots weapon control panel set the Payload profile selector to the position, corresponding to the current payload: shooting is possible from only those hardpoints, which have the symbol in the payload profile
                        </div>
                        <div className='right'>
                            <img src={img_four_position_switch} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Four Position Switch</div>
                    <div className='container'>
                        <div className='left'>
                            <div>a) Internal 800 External - to fire AP-30 on hardpoints 1,6,2,5 simultaneously or 1,6 if no AP-30 on hardpoints 2, 5</div>
                            <div>b) 800 Internal or 624 - to fire 12.7mm maschine guns on hardpoints 2, 5</div>
                            <div>c) 622 - to fire 7.62mm maschine guns on hardpoints 2,5</div>
                        </div>
                        <div className='right'>
                            <img src={img_gun_camera} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Fire</div>
                    <div className='container'>
                        <div className='left'>
                            Press fire button
                        </div>
                        <div className='right'>
                            <img src={img_fire_button} alt='' />
                        </div>
                    </div>
                </li>
            </ol>

        </ul>
    </div>);
}

export default MG_12_7_GL_30;