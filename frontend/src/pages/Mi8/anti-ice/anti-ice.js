import React, { Component } from 'react';
import '../Mi8-subpage.scss';
import img_anti_ice_automatic from './pictures/img_anti_ice_automatic.png';
import img_anti_ice_cb from './pictures/img_anti_ice_cb.png';
import img_anti_ice_control_panel from './pictures/img_anti_ice_control_panel.png';
import img_anti_ice_deenergize from './pictures/img_anti_ice_deenergize.png';
import img_anti_ice_manual from './pictures/img_anti_ice_manual.png';
import img_wipers_cb from './pictures/img_wipers_cb.png';
import img_wipers from './pictures/img_wipers.png';

const AntiIce = () => {
    return (<div className='Mi8-subpage'>
        <ul className='fast-instructions'>
            <li>Anti ice systems
                <ol>
                    <li>Heated glasses</li>
                    <li>Anti-ice system of air inlet particle separator system</li>
                    <li>Rotor anti-ice system</li>
                </ol>
            </li>
        </ul>
        <ul className='instructions'>
            <li className='instruction'>Enabling Anti-Ice system</li>
            <ol className='sub-instructions'>
                <li className='sub-instruction'>
                    <div className='desc'>Circuit breakers - Up</div>
                    <div className='container'>
                        <div className='left'>
                            <strong>--</strong>
                        </div>
                        <div className='right'>
                            <img src={img_anti_ice_cb} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Anti-Ice control panel</div>
                    <div className='container'>
                        <div className='left'>
                            <ol>
                                <li>Switch GENERAL MANUAL - AUTO</li>
                                <li>Anti-ice disabling button</li>
                                <li>Switch for PSS and left engine inlet heating</li>
                                <li>Icing indicator lamp (red) and anti-ice system activation lamp (green)</li>
                                <li>Rotary switch for consumers current monitoring</li>
                                <li>Switch for PSS and right engine inlet heating</li>
                                <li>Monitoring of detector PNO-3 heating</li>
                                <li>Switch of detector PNO-3 heating</li>
                                <li>Glasses heating switch</li>
                                <li>Detector PNO-3 heating serviceability indicator lamp</li>
                                <li>AC emperemeter</li>
                                <li>Lamp indicating successive activation of main and tail rotors separate sections</li>
                            </ol>
                        </div>
                        <div className='right'>
                            <img src={img_anti_ice_control_panel} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Anti-ice in AUTOMATIC mode</div>
                    <div className='container'>
                        <div className='left'>
                            <div>activated on signal from ice detector</div>
                            <div>all switches in lower position</div>
                        </div>
                        <div className='right'>
                            <img src={img_anti_ice_automatic} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Anti-ice in MANUAL mode</div>
                    <div className='container'>
                        <div className='left'>
                            <strong>--</strong>
                        </div>
                        <div className='right'>
                            <img src={img_anti_ice_manual} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Turning off anti-ice system - DEENERGIZE</div>
                    <div className='container'>
                        <div className='left'>
                            <div>All switches down and press button</div>
                        </div>
                        <div className='right'>
                            <img src={img_anti_ice_deenergize} alt='' />
                        </div>
                    </div>
                </li>
            </ol>
            <li className='instruction'>Using Wipers</li>
            <ol className='sub-instructions'>
                <li className='sub-instruction'>
                    <div className='desc'>Wipers - Circiut breakers - UP</div>
                    <div className='container'>
                        <div className='left'>
                            <strong>--</strong>
                        </div>
                        <div className='right'>
                            <img src={img_wipers_cb} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Enable Wipers</div>
                    <div className='container'>
                        <div className='left'>
                            <ol>
                                <li>Start - starting operating, the switch should be set to this position, for a short time</li>
                                <li>Reset - position to stop operation</li>
                                <li>Speed 1</li>
                                <li>Speed 2</li>
                            </ol>
                        </div>
                        <div className='right'>
                            <img src={img_wipers} alt='' />
                        </div>
                    </div>
                </li>
            </ol>

        </ul>
    </div>);
}

export default AntiIce;