import React, { Component } from 'react';
import '../Mi8-subpage.scss';
// import img_copilots_weapon_control_panel_1 from './pictures/img_copilots_weapon_control_panel_1.png';
import img_fire_button from './pictures/img_fire_button.png';
import img_gun_camera from './pictures/img_gun_camera.png';
import img_master_arm_switch from './pictures/img_master_arm_switch.png';
import img_pilots_lower_arm_panel_2 from './pictures/img_pilots_lower_arm_panel_2.png';
import img_pilots_upper_arm_panel_2 from './pictures/img_pilots_upper_arm_panel_2.png';
import img_weapons_cb from './pictures/img_weapons_cb.png';

const UPK_23_250 = () => {
    return (<div className='Mi8-subpage'>
        <ul className='fast-instructions'>
            <li>Engaging B8v20 rockets - overview
                <ol>
                    <li>Circuit breakers - ON</li>
                    <li>Master Arm Switch - ON</li>
                    <li>Pilot's upper arm panel</li>
                    <li>Pilot's lower arm panel</li>
                    <li>Gun Camera - optional</li>
                    <li>Fire</li>
                </ol>
            </li>
        </ul>
        <ul className='instructions'>
            <li className='instruction'>Deploying UPK-23-250 23mm gun container</li>
            <ol className='sub-instructions'>
                <li className='sub-instruction'>
                    <div className='desc'>Circuit Brakers - UP</div>
                    <div className='container'>
                        <div className='left'>
                            <strong>--</strong>
                        </div>
                        <div className='right'>
                            <img src={img_weapons_cb} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Enable master arm switch</div>
                    <div className='container'>
                        <div className='left'>
                            <strong>--</strong>
                        </div>
                        <div className='right'>
                            <img src={img_master_arm_switch} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Pilot's Upper Armament Panel</div>
                    <div className='container'>
                        <div className='left'>
                            <div>a) PRESS check lamps button</div>
                            <div>b) CHECK LIGHTS - that all lamps are on</div>
                            <div>c) ENABLE RKT Gun maser switch and check that hardpoints 2, 5 are loaded</div>
                            <div>d) CHECK lights are on, hardpoints 2, 5 are loaded</div>
                            <div>e) CHECK lights are on, rockets launching circuit</div>
                        </div>
                        <div className='right'>
                            <img src={img_pilots_upper_arm_panel_2} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Pilot's Lower Armament Panel</div>
                    <div className='container'>
                        <div className='left'>
                            <div>a) SWITCH UP - connect upk-25-250 firing circuits to the combat button</div>
                        </div>
                        <div className='right'>
                            <img src={img_pilots_lower_arm_panel_2} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Gun Camera (optionally)</div>
                    <div className='container'>
                        <div className='left'>
                            (optionally)
                            Enable gun camera, player must enable gun camera functionality
                        </div>
                        <div className='right'>
                            <img src={img_gun_camera} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Fire</div>
                    <div className='container'>
                        <div className='left'>
                            Press fire button
                        </div>
                        <div className='right'>
                            <img src={img_fire_button} alt='' />
                        </div>
                    </div>
                </li>
            </ol>

        </ul>
    </div>);
}

export default UPK_23_250;