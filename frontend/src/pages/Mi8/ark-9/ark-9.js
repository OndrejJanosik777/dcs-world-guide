import React, { Component } from 'react';
import '../Mi8-subpage.scss';
import img_ark_9_control_panel_overview from './pictures/img_ark_9_control_panel_overview.png';
import img_ark_9_control_panel from './pictures/img_ark_9_control_panel.png';
import img_source_selector_ark_9 from './pictures/img_source_selector_ark_9.png';
import img_ugr_4uk_needle_left from './pictures/img_ugr_4uk_needle_left.png';

const Ark_9 = () => {
    return (<div className='Mi8-subpage'>
        <ul className='fast-instructions'>
            <li>Using ARK-9 Radio Navigation
                <ol>
                    <li>Turn on circuit breakers</li>
                    <li>Source Selector - PK1</li>
                    <li>ICS RADIO Selector - DOWN</li>
                    <li>Set Switch UGR-4UK - LEFT</li>
                    <li>ARK 9 Control Panel</li>
                </ol>
            </li>
        </ul>
        <ul className='instructions'>
            <li className='instruction'>Using ARK-9 Radio Navigation</li>
            <ol className='sub-instructions'>
                <li className='sub-instruction'>
                    <div className='desc'>Circuit breakers - Up</div>
                    <div className='container'>
                        <div className='left'>
                            <strong>--</strong>
                        </div>
                        <div className='right'>
                            <strong>--</strong>
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Source Selector - PK1</div>
                    <div className='container'>
                        <div className='left'>
                            <div>a) SET Source Selector to PK1</div>
                            <div>b) ICS-RADIO Selector to DOWN</div>
                        </div>
                        <div className='right'>
                            <img src={img_source_selector_ark_9} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Switch UGR-4UK</div>
                    <div className='container'>
                        <div className='left'>
                            <div>a) Set Switch UGR-4UK - LEFT</div>
                        </div>
                        <div className='right'>
                            <img src={img_ugr_4uk_needle_left} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>ARK-9 Control Panel</div>
                    <div className='container'>
                        <div className='left'>
                            <div>a) mode selector to ANT (3rd)</div>
                            <div>b) voice cw to (CW) down</div>
                            <div>c) volume control to maximum</div>
                            <div>d) B - D channel to B - LEFT</div>
                        </div>
                        <div className='right'>
                            <img src={img_ark_9_control_panel} alt='' />
                        </div>
                    </div>
                </li>
            </ol>
            <li className='instruction'>ARK 9 Control Panel overview</li>
            <ol className='sub-instructions'>
                <li className='sub-instruction'>
                    <div className='desc'>ARK 9 Control Panel</div>
                    <div className='container'>
                        <div className='left'>
                            <div>1) voice CW Switch</div>
                            <div>2) Signal Power indicator</div>
                            <div>3) OFF - COMP - ANT - LOOP</div>
                            <div>4) LOOP spring load switch</div>
                            <div>5) volume control</div>
                            <div>6) B - D channel switch</div>
                            <div>7-9) frequency setting dials</div>
                        </div>
                        <div className='right'>
                            <img src={img_ark_9_control_panel_overview} alt='' />
                        </div>
                    </div>
                </li>
            </ol>
        </ul>
    </div>);
}

export default Ark_9;