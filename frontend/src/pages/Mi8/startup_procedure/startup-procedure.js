import React, { Component } from 'react';
import '../Mi8-subpage.scss';
import img_anti_ice_circuit_breakers from './pictures/anti_ice_circuit_breakers.png';
import img_apu_generator_on from './pictures/apu_generator_on.png';
import img_apu_off from './pictures/apu_off.png';
import img_circuit_breakers from './pictures/circuit_breakers.png';
import img_cyclic_center from './pictures/cyclic_center.png';
import img_engine_power_levers_middle from './pictures/engine_power_levers_middle.png';
import img_engine_start from './pictures/engine_start.png';
import img_fuel_lever_open from './pictures/fuel_lever_open.png';
import img_fuel_pumps from './pictures/fuel_pumps.png';
import img_generators_and_rectifiers_on from './pictures/generators_and_rectifiers_on.png';
import img_generators_down from './pictures/generators_down.png';
import img_3K_momentary_switch from './pictures/img_3K_momentary_switch.png';
import img_altitude_indicator from './pictures/img_altitude_indicator.png';
import img_avionics_left from './pictures/img_avionics_left.png';
import img_avionics_right from './pictures/img_avionics_right.png';
import img_directional_gyro from './pictures/img_directional_gyro.png';
import img_migalka from './pictures/img_migalka.png';
import img_test_autopilot from './pictures/img_test_autopilot.png';
import img_inverters_auto from './pictures/inverters_auto.png';
import img_inverters from './pictures/inverters.png';
import img_open_fuel_valves from './pictures/open_fuel_valves.png';
import img_particle_separators from './pictures/particle_separators.png';
import img_rotor_bracke_down from './pictures/rotor_bracke_down.png';
import img_run_apu from './pictures/run_apu.png';
import img_start_apu from './pictures/start_apu.png';
import img_test_fire_signal from './pictures/test_fire_signal.png';
import img_throttle_full_left from './pictures/throttle_full_left.png';
import img_throttle_right from './pictures/throttle_right.png';
import img_turn_on_electrical_power from './pictures/turn_on_electrical_power.png';


const StartupProcedure = () => {
    return (<div className='Mi8-subpage'>
        <ul className='fast-instructions'>
            <li>Preflight cockpit check
                <ol>
                    <li>Rotor Brake - Down</li>
                    <li>Cyclic - Center</li>
                    <li>Collective - Down</li>
                    <li>Throttle - Full left</li>
                    <li>Engine Power Levers - Middle</li>
                </ol>
            </li>
            <li>Preparations preceeding APU Start
                <ol>
                    <li>Battery 1,2 - On</li>
                    <li>Set Inverters AC ~115V, ~36V - Manual</li>
                    <li>Circuit breakers - On</li>
                </ol>
            </li>
            <li>Starting APU
                <ol>
                    <li>Test Fire Signal - OFF</li>
                    <li>Fuel Pumps - ON</li>
                    <li>Shutoff Valves - UP</li>
                    <li>APU - Start</li>
                </ol>
            </li>
            <li>Starting the main engines
                <ol>
                    <li>Start First Engine</li>
                    <li>Open First Fuel Lever</li>
                    <li>Start Second Engine</li>
                    <li>Open Second Fuel Lever</li>
                    <li>Particle Separators - ON</li>
                    <li>APU - OFF</li>
                </ol>
            </li>
            <li>Engine run-up
                <ol>
                    <li>Throttle - RIGHT</li>
                    <li>Generators - ON</li>
                    <li>Rectifiers - ON</li>
                    <li>Set Inverters AC ~115V, ~36V - Auto</li>
                </ol>
            </li>
            <li>Turn on Avionics
                <ol>
                    <li>Altitude Indicator - On</li>
                    <li>Avionics Panel Left - ON</li>
                    <li>Avionics Panel Right - ON</li>
                    <li>Gyromagnetic compas - ON</li>
                    <li>Autopilot - ON</li>
                    <li>Migalka System - ON (UP)</li>
                </ol>
            </li>
        </ul>
        <ul className='instructions'>
            <li className='instruction'>Preflight cockpit check</li>
            <ol className='sub-instructions'>
                <li className='sub-instruction'>
                    <div className='desc'>Rotor Brake - DOWN</div>
                    <div className='container'>
                        <div className='left'>RCtrl+R</div>
                        <div className='right'>
                            <img src={img_rotor_bracke_down} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Cyclic - CENTER</div>
                    <div className='container'>
                        <div className='left'>
                            <div>For activate indicator LCtrl+Enter</div>
                        </div>
                        <div className='right'>
                            <img src={img_cyclic_center} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Collective - DOWN</div>
                    <div className='container'>
                        <div className='left'>Num -</div>
                        <div className='right'>
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Throttle - FULL LEFT</div>
                    <div className='container'>
                        <div className='left'></div>
                        <div className='right'>
                            <img src={img_throttle_full_left} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Engine Power Levers - MIDDLE</div>
                    <div className='container'>
                        <div className='left'></div>
                        <div className='right'>
                            <img src={img_engine_power_levers_middle} alt='' />
                        </div>
                    </div>
                </li>
            </ol>
            <li className='instruction'>Preparations preceeding APU Start</li>
            <ol className='sub-instructions'>
                <li className='sub-instruction'>
                    <div className='desc'>Battery - ON</div>
                    <div className='container'>
                        <div className='left'>
                            <div>Right Side Panel</div>
                            <strong>Battery 1,2 - ON</strong>
                            <div>Check the voltage of the battery bus by setting the DC selector knob to
                                BATT BUS. The voltage should be no less than 24 V.
                            </div>
                        </div>
                        <div className='right'>
                            <img src={img_turn_on_electrical_power} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>AC ~115V, ~36V - Manual</div>
                    <div className='container'>
                        <div className='left'>
                            <div>Right Rear Console</div>
                            <strong>AC: ~115V, ~36V - MANUAL</strong>
                            <div>Note: When cold start from batteries, to prevent them from fast discharge, it is
                                recommended to leave the ~36V inverter switch in neutral position, until generators
                                are operating.
                            </div>
                        </div>
                        <div className='right'>
                            <img src={img_inverters} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Circuit Break Panels - ON</div>
                    <div className='container'>
                        <div className='left'>
                            <div>Right Rear Console</div>
                            <strong>All circuit breakers - ON</strong>
                            <div>Note: 1-3 for Weapon Systems.
                            </div>
                        </div>
                        <div className='right'>
                            <img src={img_circuit_breakers} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Anti-ice - OFF (unless required)</div>
                    <div className='container'>
                        <div className='left'>
                            <div>Anti-ice Control, Left Engine, Right Engine circuit breakers - Off
                                (unless required)
                            </div>
                        </div>
                        <div className='right'>
                            <img src={img_anti_ice_circuit_breakers} alt='' />
                        </div>
                    </div>
                </li>
            </ol>
            <li className='instruction'>Starting the APU</li>
            <ol className='sub-instructions'>
                <li className='sub-instruction'>
                    <div className='desc'>Test Fire Signal - OFF</div>
                    <div className='container'>
                        <div className='left'>
                            <div>Set the knob of TEST Fire signal channel switch to OFF, then switch to EXT, up -light out
                            </div>
                        </div>
                        <div className='right'>
                            <img src={img_test_fire_signal} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Fuel boost and fuel transfer pumps - ON</div>
                    <div className='container'>
                        <div className='left'>
                            <div>Switch on the fuel boost pumps of the service tank and the fuel transfer pumps of the main tanks.
                                <div><strong> RShift + 1,2,3 </strong></div>
                            </div>
                            <div>Note: When performing cold start from batteries, it is recommended not to enable transfer pumps, until APU generator or
                                rectifiers are operational (picture at the bottom).
                            </div>
                        </div>
                        <div className='right'>
                            <img src={img_fuel_pumps} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Open the fuel fire (shutoff) valves - UP</div>
                    <div className='container'>
                        <div className='left'>
                            <div>Open the fuel fire valves - useDispatch
                                <div>
                                    <strong>LAlt + 5,6</strong>
                                    <strong>RAlt + 5,6</strong>
                                </div>
                            </div>
                        </div>
                        <div className='right'>
                            <img src={img_open_fuel_valves} alt='' />
                        </div>
                    </div>
                </li>

                <li className='sub-instruction'>
                    <div className='desc'>Start the APU Control Panel - UP</div>
                    <div className='container'>
                        <div className='left'>
                            <div>-
                            </div>
                        </div>
                        <div className='right'>
                            <img src={img_start_apu} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Start APU</div>
                    <div className='container'>
                        <div className='left'>
                            <div>Press START button for 2-3 seconds. Auto Ignition light should iluminate.
                                The APU Accelerates to idle speed indicated by oil pressure normal and normal speed lights.
                                Time to reach idle speed should not exceed 20 seconds.
                            </div>
                            <div>
                                Continuous EGT does not exceed 720°C
                            </div>
                        </div>
                        <div className='right'>
                            <img src={img_run_apu} alt='' />
                        </div>
                    </div>
                </li>
            </ol>
            <li className='instruction'>Starting the main engines</li>
            <ol className='sub-instructions'>
                <li className='sub-instruction'>
                    <div className='desc'>Starting Main Engine</div>
                    <div className='container'>
                        <div className='left'>
                            <div>The engines starting order depends on wind direction. The engine on the downwind side is started first.</div>
                            <div>Set Start Mode Selector - UP</div>
                            <div>Select engine to start - LEFT or RIGHT</div>
                        </div>
                        <div className='right'>
                            <img src={img_engine_start} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Fuel Lever - OPEN</div>
                    <div className='container'>
                        <div className='left'>
                            <div>Set forward the fuel shutoff lever of the engine beeing started - left or right.</div>
                            <div>The engine should reach idle speed in 60 seconds and lights should iluminate during the start.</div>
                        </div>
                        <div className='right'>
                            <img src={img_fuel_lever_open} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Particle separators - ON</div>
                    <div className='container'>
                        <div className='left'>
                            <div>Immediately after starting the engine switch on the PZU particle separators for both engines to - ON.
                                Check light iluminate up to 30 seconds.
                            </div>
                        </div>
                        <div className='right'>
                            <img src={img_particle_separators} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>APU - OFF</div>
                    <div className='container'>
                        <div className='left'>
                            <div>After succesfully starting the main enginer, allow the APU to cool down at idle speed for 0.5 - 1.0 min and shut the APU down
                                by pressing the APU OFF button.
                            </div>
                        </div>
                        <div className='right'>
                            <img src={img_apu_off} alt='' />
                        </div>
                    </div>
                </li>
            </ol>
            <li className='instruction'>Engine run-up, switching generators and rectifiers - ON</li>
            <ol className='sub-instructions'>
                <li className='sub-instruction'>
                    <div className='desc'>Throttle - RIGHT</div>
                    <div className='container'>
                        <div className='left'>
                            <div>rotate throttle full right
                            </div>
                        </div>
                        <div className='right'>
                            <img src={img_throttle_right} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Generators, Rectifiers - ON</div>
                    <div className='container'>
                        <div className='left'>
                            <div>Set the AC generators 1,2 and rectifiers switches 1,2,3 to ON
                            </div>
                        </div>
                        <div className='right'>
                            <img src={img_generators_and_rectifiers_on} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Set Inventers ~115, ~36 to AUTO</div>
                    <div className='container'>
                        <div className='left'>
                            <div>Set inventers to AUTO (DOWN position)
                            </div>
                        </div>
                        <div className='right'>
                            <img src={img_inverters_auto} alt='' />
                        </div>
                    </div>
                </li>
            </ol>
            <li className='instruction'>Avionics - On</li>
            <ol className='sub-instructions'>
                <li className='sub-instruction'>
                    <div className='desc'>Altitude Indicator</div>
                    <div className='container'>
                        <div className='left'>
                            <div>cage the device by pressing the cage button</div>
                            <div>switch on test altitude indicator</div>
                        </div>
                        <div className='right'>
                            <img src={img_altitude_indicator} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Avionics on left panel - switch on</div>
                    <div className='container'>
                        <div className='left'>
                            <div>switch on gyro correction cutout switch</div>
                            <div>PITH LIM SYS</div>
                            <div>INF REP</div>
                        </div>
                        <div className='right'>
                            <img src={img_avionics_right} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Avionics on left panel - switch on</div>
                    <div className='container'>
                        <div className='left'>
                            <div>doppler navigation system</div>
                            <div>the right AGB-3K: pressing the CAGE</div>
                            <div>gyromagnetic compass</div>
                        </div>
                        <div className='right'>
                            <img src={img_avionics_left} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>gyromagnetic compass</div>
                    <div className='container'>
                        <div className='left'>
                            <div>When directional gyro heading arrow settles on the starting ground course, set gyromagnetic compass to (directional gyro) mode</div>
                        </div>
                        <div className='right'>
                            <img src={img_directional_gyro} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>test autopilot</div>
                    <div className='container'>
                        <div className='left'>
                            <div>test autopilot by pressing button-lamps on the autopilot control panel</div>
                            <div>yaw channel</div>
                            <div>roll-pitch channel</div>
                            <div>altitude channel</div>
                        </div>
                        <div className='right'>
                            <img src={img_test_autopilot} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>3K momentary switch</div>
                    <div className='container'>
                        <div className='left'>
                            <div>Witch feet of the pedals, press 3K momentary switch on PU-26 control panel for a short time to the left or right</div>
                            <div>The Yaw channel scale on the autopilot should rotate in response to the manual heading change input</div>
                        </div>
                        <div className='right'>
                            <img src={img_3K_momentary_switch} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Enable migalka (flash) system</div>
                    <div className='container'>
                        <div className='left'>
                            <div>-</div>
                        </div>
                        <div className='right'>
                            <img src={img_migalka} alt='' />
                        </div>
                    </div>
                </li>
            </ol>
        </ul>
    </div>);
}

export default StartupProcedure;