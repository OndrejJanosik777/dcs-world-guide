import React, { Component } from 'react';
import '../Mi8-subpage.scss';
import img_batteries_off from './pictures/img_bateries_off.png';
import img_circuit_breakers_off from './pictures/img_circuit_breakers_off.png';
import img_engage_rotor_brake from './pictures/img_engage_rotor_brake.png';
import img_flight_recorder_auto from './pictures/img_flight_recorder_auto.png';
import img_fuel_boost_off from './pictures/img_fuel_boost_off.png';
import img_fuel_shutoff_levers_off from './pictures/img_fuel_shutoff_levers_off.png';
import img_fuel_valves_off from './pictures/img_fuel_valves_off.png';
import img_particle_separator_off from './pictures/img_particle_separator_off.png';
import img_rectifiers_generator_off from './pictures/img_rectifiers_generator_off.png';
import img_switch_off_avionics from './pictures/img_switch_off_avionics.png';
import img_throttle_left from './pictures/img_throttle_left.png';


const EngineShutdown = () => {
    return (<div className='Mi8-subpage'>
        <ul className='fast-instructions'>
            <li>Preflight cockpit check
                <ol>
                    <li>Particle Separators - OFF</li>
                    <li>Avionics - OFF</li>
                    <li>Rectifiers - OFF</li>
                    <li>Set Inverters AC ~115V, ~36V - UP</li>
                    <li>Generators - OFF</li>
                    <li>Throttle - LEFT</li>
                    <li>Rotor Brake - Engage (less than 15% Power)</li>
                    <li>Fuel Valves - OFF</li>
                    <li>Fuel Boost - OFF</li>
                    <li>Cicuit Brakes - OFF</li>
                    <li>Batteries - OFF</li>
                    <li>Flight Recorder - Auto</li>
                </ol>
            </li>
        </ul>
        <ul className='instructions'>
            <li className='instruction'>Shutting down the engine</li>
            <ol className='sub-instructions'>
                <li className='sub-instruction'>
                    <div className='desc'>Particle separators - OFF</div>
                    <div className='container'>
                        <div className='left'>--</div>
                        <div className='right'>
                            <img src={img_particle_separator_off} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Avionics - OFF</div>
                    <div className='container'>
                        <div className='left'>
                            switch off all electrical power consumers apart from powerplant monitoring and control systems (left and right side)
                        </div>
                        <div className='right'>
                            <img src={img_switch_off_avionics} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Rectifiers, Generators - OFF</div>
                    <div className='container'>
                        <div className='left'>
                            <div>Switch off the rectifiers</div>
                            <div>Set inverter switch to manual - UP</div>
                            <div>switch off the AC generators</div>
                        </div>
                        <div className='right'>
                            <img src={img_rectifiers_generator_off} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Throttle - LEFT</div>
                    <div className='container'>
                        <div className='left'>
                            <div>Turn the throttle full left</div>
                        </div>
                        <div className='right'>
                            <img src={img_throttle_left} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Fuel shutoff levers - CLOSE</div>
                    <div className='container'>
                        <div className='left'>
                            <div>After allowing the engines a 2 minute cool down period in idle power, close the fuel shutoff levers</div>
                        </div>
                        <div className='right'>
                            <img src={img_fuel_shutoff_levers_off} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Rotor Brake - Engage</div>
                    <div className='container'>
                        <div className='left'>
                            <div>After Nr less than 15% - engage rotor brake </div>
                        </div>
                        <div className='right'>
                            <img src={img_engage_rotor_brake} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Fuel Valves - OFF</div>
                    <div className='container'>
                        <div className='left'>
                            <div>With engines fully stopped, switch off the fuel fire (shutoff) valves</div>
                        </div>
                        <div className='right'>
                            <img src={img_fuel_valves_off} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Fuel boost - OFF</div>
                    <div className='container'>
                        <div className='left'>
                            <div>switch off fuel boost and transfer pumps</div>
                        </div>
                        <div className='right'>
                            <img src={img_fuel_boost_off} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Circuit brakers - OFF</div>
                    <div className='container'>
                        <div className='left'>
                            <div>Switch off all circuit brakers and all other control switches of OFF position, apart
                                from the reserve hydraulic system
                            </div>
                        </div>
                        <div className='right'>
                            <img src={img_circuit_breakers_off} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Bateries - OFF</div>
                    <div className='container'>
                        <div className='left'>
                            <div>Switch Off the bateries</div>
                        </div>
                        <div className='right'>
                            <img src={img_batteries_off} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Flight Recorder - Auto</div>
                    <div className='container'>
                        <div className='left'>
                            <div>Set the flight recorder to Auto</div>
                        </div>
                        <div className='right'>
                            <img src={img_flight_recorder_auto} alt='' />
                        </div>
                    </div>
                </li>
            </ol>

        </ul>
    </div>);
}

export default EngineShutdown;