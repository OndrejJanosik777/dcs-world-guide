import React, { Component } from 'react';
import '../Mi8-subpage.scss';
import img_cargo_select from './pictures/img_cargo_select.png';
import img_external_cargo_indicator from './pictures/img_external_cargo_indicator.png';
import img_usefull_info from './pictures/img_usefull_info.png';
import img_view_from_blister from './pictures/img_view_from_blister.png';

const SlingOP = () => {
    return (<div className='Mi8-subpage'>
        <ul className='fast-instructions'>
            <li>Engaging B8v20 rockets - overview
                <ol>
                    <li>Circuit breakers</li>
                    <li>Master Arm Switch</li>
                    <li>Pilot's upper arm panel</li>
                    <li>Pilot's lower arm panel</li>
                    <li>Gun Camera - optional</li>
                    <li>Copilot's weapon control panel</li>
                    <li>Fire</li>
                </ol>
            </li>
        </ul>
        <ul className='instructions'>
            <li className='instruction'>Sling Operations</li>
            <ol className='sub-instructions'>
                <li className='sub-instruction'>
                    <div className='desc'>Selecting Cargo</div>
                    <div className='container'>
                        <div className='left'>
                            Press F6 and F1 ... F10
                        </div>
                        <div className='right'>
                            <img src={img_cargo_select} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Ëxternal cargo indicator</div>
                    <div className='container'>
                        <div className='left'>
                            <div>To activate/deactivate<strong>RCTRL+RSHIFT+P</strong></div>
                            <div>1) projection of cargo hook</div>
                            <div>2) cargo position</div>
                            <div>3) allowed hover altitude</div>
                            <div>4) indicator of helicopter‘s altitude</div>
                        </div>
                        <div className='right'>
                            <img src={img_external_cargo_indicator} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Pilot's blister</div>
                    <div className='container'>
                        <div className='left'>
                            <div>To activate/deactivate<strong>LSHIFT+LALT+C</strong></div>
                        </div>
                        <div className='right'>
                            <img src={img_view_from_blister} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Usefull Info</div>
                    <div className='container'>
                        <div className='left'>
                            <div>To activate/deactivate<strong>RSHIFT+K</strong></div>
                        </div>
                        <div className='right'>
                            <img src={img_usefull_info} alt='' />
                        </div>
                    </div>
                </li>
            </ol>
        </ul>
    </div>);
}

export default SlingOP;