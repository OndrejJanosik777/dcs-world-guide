import React, { Component } from 'react';
import '../../../Mi8-subpage.scss';
import img_mission_1_cover from './pictures/img_mission_1_cover.png';
import img_mission_1_detail_1 from './pictures/img_mission_1_detail_1.png';
import img_mission_1_detail_2 from './pictures/img_mission_1_detail_2.png';
import img_mission_1_detail_3 from './pictures/img_mission_1_detail_3.png';
import img_mission_1_detail_4 from './pictures/img_mission_1_detail_4.png';
import img_mission_1_map from './pictures/img_mission_1_map.png';

const Mission_01 = () => {
    return (<div className='Mi8-subpage'>
        <ul className='fast-instructions'>
        </ul>
        <ul className='instructions'>
            <li className='instruction'>MISSION 1 : NEVER FORGET </li>
            <ol className='sub-instructions'>
                <li className='sub-instruction'>
                    <div className='desc'>SITUATION</div>
                    <div className='container_1row'>
                        <div className='left'>
                            The Second World War took the lives of more than 26 million citizens of the Soviet Union. Nearly 5 million people are still missing. They are not alive and they are not among the dead.15th of July 2008.Nalchik. Kabardino-Balkaria.Task:Deliver a unit of the 34th special mountain motorized rifle brigade to the eastern slope of Mount Elbrus, to the restored hotel from the 2nd World War - Shelter 11 (4240m) The unit was sent with the aim of finding and repatriating the remains of the Red Army soldiers who died in facing overwhelming odds in battles with Nazi soldiers — with the Edelweiss unit.
                        </div>
                        <div className='full_width'>
                            <img src={img_mission_1_cover} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>MAP</div>
                    <div className='container_1row'>
                        <div className='left'>
                            <div><strong>MISSION OVERVIEW</strong></div>
                            <div>Start at 15/7/2008  08:10:00</div>
                            <div>Expected duration: 45 min</div>
                            <div><strong>MISSION DATA</strong></div>
                            <div>Task: Transport</div>
                            <div>Flight: Mi-8MT*1</div>
                            <div>Fuel: 2765 lbs</div>
                            <div>Weapon No Weapon</div>
                            <div>TASKS:</div>
                            <div>Takeoff RWY NALCHIK 136.00mhz</div>
                            <div>Landing- Shelter 11 call sign KURGAN 132.50mhz</div>
                            <div>Landing- RWY NALCHIK 136.00mhz</div>
                        </div>
                        <div className='full_width'>
                            <img src={img_mission_1_map} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Initial Waypoint</div>
                    <div className='container_1row'>
                        <div className='left'>
                            PWY NALCHIK
                        </div>
                        <div className='full_width'>
                            <img src={img_mission_1_detail_1} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Maximum volume</div>
                    <div className='container_1row'>
                        <div className='left'>
                            --
                        </div>
                        <div className='full_width'>
                            <img src={img_mission_1_detail_2} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>LANDING SITE</div>
                    <div className='container_1row'>
                        <div className='left'>
                            --
                        </div>
                        <div className='full_width'>
                            <img src={img_mission_1_detail_3} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Correct position for Landing</div>
                    <div className='container_1row'>
                        <div className='left'>
                            --
                        </div>
                        <div className='full_width'>
                            <img src={img_mission_1_detail_4} alt='' />
                        </div>
                    </div>
                </li>
            </ol>
        </ul>
    </div>);
}

export default Mission_01;