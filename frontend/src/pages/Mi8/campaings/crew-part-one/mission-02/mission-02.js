import React, { Component } from 'react';
import '../../../Mi8-subpage.scss';
import img_mission_2_heli_description from './pictures/img_mission_2_heli_description.png';
import img_mission_2_heli from './pictures/img_mission_2_heli.png';
import img_mission_2_map_detail_1 from './pictures/img_mission_2_map_detail_1.png';
import img_mission_2_map_detail_2 from './pictures/img_mission_2_map_detail_2.png';
import img_mission_2_map from './pictures/img_mission_2_map.png';

const Mission_02 = () => {
    return (<div className='Mi8-subpage'>
        <ul className='fast-instructions'>
        </ul>
        <ul className='instructions'>
            <li className='instruction'>MISSION 2 : VIP PASSENGER </li>
            <ol className='sub-instructions'>
                <li className='sub-instruction'>
                    <div className='desc'>SITUATION</div>
                    <div className='container_1row'>
                        <div className='left'>
                            On New Years Eve our crew has received orders. Sergey Viktorovich Safronov, the CEO of PJSC Gazprom-Yuzhneft is arriving shortly.
                            He is also one of the vice-presidents of the parent company, Gazprom. He is the main VIP aboard this comfortable aircraft.
                            We have our flight schedule planned for the next 3 days. Finally, we are getting some flight time! Compared to other aircraft,
                            Tarasov’s crew didn’t log much flight time and only had 12 sorties during 2015. The boys are eager to get airborne.For the commander,
                            the coming year is shaping up to be truly magical and, hopefully, bring a welcome promotion.Before the flight to the gas production sites,
                            Safronov held a briefing with the Gazprom-Yuzhneft aviation squad. Yearly reports were given, financial and technical details and equipment
                            readiness summaries were provided to him. Dmitriy, commander of the aircraft 22051, requested a privatemeeting. Tarasov requested a promotion
                            with aviation squad XO present. Safronov was hesitant and promised to address the issue after the holidays.The much-anticipated promotion is a transfer
                            to Moscow-based aviation branch of “Gazprom” to a brand-new MI-171A in a VIP transport configuration. For Dmitriy this is the pinnacle of his career.
                            A roomy apartment in Moscow, salary twice-and-a-half times his current one, reporting directly to head corporate staff. Only flying for the government is better than this.
                            “What can I say? We’re finally going to do some flying. We’ll work something out then.” - Sergey Viktorovich concluded cautiously.Safronov isn’t in a hurry.
                            He wants to make the right choice as he is going to bear personal responsibility for this assignment. Miracles don’t happen in our world and Dmitriy has to work
                            hard to prove himself worthy.TASK: Follow the flight plan along the central gas production sites. At Site #1 and #2 you are to pick-up 4 additional passengers.
                            Final destination is VP Amulet.There are plenty of divert fields available -Gelendzhik - 126MhzNovorossiysk - 123MhzVP Offshore Gas Boring Platform (MBU) “Kapel”’ - 123.5Mhz
                        </div>
                        <div className='full_width'>
                            <img src={img_mission_2_heli} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>HELICOPTER</div>
                    <div className='container_1row'>
                        <div className='left'>
                            <div><strong>MISSION OVERVIEW</strong></div>
                            <div>Start at 29/12/2015 09:54:00</div>
                            <div>Expected duration: 45 min</div>
                            <div><strong>MISSION DATA</strong></div>
                            <div>Task: Transport</div>
                            <div>Flight: Mi-8MT*1</div>
                            <div>Fuel: 2765 lbs</div>
                            <div>Weapon No Weapon</div>
                        </div>
                        <div className='full_width'>
                            <img src={img_mission_2_heli_description} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>MAP</div>
                    <div className='container_1row'>
                        <div className='left'>
                            <div>Take-off: VP Amulet 124.6Mhz (OPRS-707Khz)</div>
                            <div>Landing: Maryna Roscha CDNG2 (No UVD control provided)</div>
                            <div>Landing: Offshore Gas Boring Platform (MBU) CNDG1, VP Otrkytka 123.5Mhz (OPRS-767Khz)</div>
                            <div>Final Destination Landing: VP Amulet 124.6Mhz (OPRS-707Khz)</div>

                            <div>The average speed on the route is 190km / h</div>
                            <div>The average flight duration is 45 min.</div>
                            <div>To maintain a comfortable and smooth flight</div>
                            <div>- do not allow a vertical speed of more than 7 m / sec.,</div>
                            <div>- do not allow a roll angle of more than 13 degrees,</div>
                            <div>- do not allow a pitch angle of more than 11 degrees,</div>
                            <div>- do not allow an overload level of more than 2.5G.</div>
                        </div>
                        <div className='full_width'>
                            <img src={img_mission_2_map} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Heliport - AMULET</div>
                    <div className='container_1row'>
                        <div className='left'>
                            Detail on Heliport AMULET
                        </div>
                        <div className='full_width'>
                            <img src={img_mission_2_map_detail_1} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Heliport - MARINA ROSCHA</div>
                    <div className='container_1row'>
                        <div className='left'>
                            Detail on Heliport near MARINA ROSCHA
                        </div>
                        <div className='full_width'>
                            <img src={img_mission_2_map_detail_2} alt='' />
                        </div>
                    </div>
                </li>
            </ol>
        </ul>
    </div>);
}

export default Mission_02;