import React, { Component } from 'react';
import '../../../Mi8-subpage.scss';
import img_mission_3_cover from './pictures/img_mission_3_cover.png';
import img_mission_3_detail_1 from './pictures/img_mission_3_detail_1.png';
import img_mission_3_detail_2 from './pictures/img_mission_3_detail_2.png';
import img_mission_3_detail_3 from './pictures/img_mission_3_detail_3.png';
import img_mission_3_map from './pictures/img_mission_3_map.png';

const Mission_03 = () => {
    return (<div className='Mi8-subpage'>
        <ul className='fast-instructions'>
        </ul>
        <ul className='instructions'>
            <li className='instruction'>MISSION 3 : OLD FRIENDS</li>
            <ol className='sub-instructions'>
                <li className='sub-instruction'>
                    <div className='desc'>SITUATION</div>
                    <div className='container_1row'>
                        <div className='left'>
                            - Listen, Sanych, - Peter Mikhailovich turned to Tarasov with a smile, - Who does he think he is? He needs a comfortable flight? Including a landing on the offshore drilling rig?  ...At least we're landing, he should be happy! -Michalych, don’t be a child. You know the situation perfectly well. Help me get his support.  -We will help.  -It was quiet in the chart room. Andrei, who was the pilot navigator, was engaged in calculations and preparations for the flight.  - Of course, Dima, you surprise me too. Do you really think that in that damn Moscow someone is waiting for you?  Your home is here. Sergei, the squad leader, can cover your mistakes if you need it. - Mikhalych! – [unable to withstand Dmitriy shouts]: - You are looking at this situation from an old man’s perspective.  I was taught this way: you set a goal for yourself, judging the situation, and until you win you push for success. - You are like Makedonckiy.- Here we go again, Peter Mikhailovich. We should look at the flight plan instead.  -How are you?  When did you last see fighters at close distance?  His close friends - Air Force General Busargin and Admiral of the Navy Serebryakov, are going to visit our boss.  We need to pick them up and deliver them to AMULET.  On this occasion, Safronov arranged a military escort.  To meet a friend in the friendly and cozy atmosphere of our helicopter and immediately to hug each other for warmth. - They will hug each other for warmth all the way, and we have to ferry them from one place to another?  Remember my word, Dimka, is something that these generals do. People like this, if they rest, then it’s for the last time. Task: With the full crew, and Safronov as a passenger, fly the pre-defined route and collect two additional VIP passengers. The average speed on the route is 190km / h  The average flight duration is 60 min. To maintain a comfortable and smooth flight  - do not allow a vertical speed of more than 7 m / sec., do not allow a roll angle of more than 13 degrees, do not allow a pitch angle of more than 11 degrees, do not allow an overload level of more than 2.5G.
                        </div>
                        <div className='full_width'>
                            <img src={img_mission_3_cover} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>MAP</div>
                    <div className='container_1row'>
                        <div className='left'>
                            <div><strong>MISSION OVERVIEW</strong></div>
                            <div>Start at 31/12/2015  16:30:00</div>
                            <div>Expected duration: 60 min</div>
                            <div><strong>MISSION DATA</strong></div>
                            <div>Task: Transport</div>
                            <div>Flight: Mi-8MT*1</div>
                            <div>Fuel: 2765 lbs</div>
                            <div>Weapon No Weapon</div>
                            <div>OBJECTIVE:</div>
                            <div>Departure: HP Amulet 124.6MHz <strong>(NDB 707kHz)</strong></div>
                            <div>Landing- RWY Gelendzhik 126MHz <strong>(NDB1000kHz)</strong> !!!! ATTENTION - GELENDZHIK-TOWER 123.6MHz !!!!</div>
                            <div>Additionally, as a training and combat exercise, interact with the Russian Navy as per the approved plan:</div>
                            <div>1 Enter the training area - Not later than 17:25hrs</div>
                            <div>2 Contact ATC controller OBYECTIV - 127.5MHz</div>
                            <div>3 Landing – on aircraft carrier Admiral Kuznetsov OBYECTIV-Landing - 127.5MHz, not later than 17:40hrs.</div>
                            <div>The meeting point with the naval group is at surface NDB-beacon - <strong>(888kHz)</strong> 4 Exit the training area.</div>
                            <div>Landing - HP AMULET 124.6MHz (NDB 707kHz)</div>
                        </div>
                        <div className='full_width'>
                            <img src={img_mission_3_map} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>LANDMARK BEFORE GELENZHIK</div>
                    <div className='container_1row'>
                        <div className='left'>
                            --
                        </div>
                        <div className='full_width'>
                            <img src={img_mission_3_detail_1} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>LADING AREA - GELENZHIK</div>
                    <div className='container_1row'>
                        <div className='left'>
                            --
                        </div>
                        <div className='full_width'>
                            <img src={img_mission_3_detail_2} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>ADMIRAL KUZNETSOV</div>
                    <div className='container_1row'>
                        <div className='left'>
                            --
                        </div>
                        <div className='full_width'>
                            <img src={img_mission_3_detail_3} alt='' />
                        </div>
                    </div>
                </li>
            </ol>
        </ul>
    </div>);
}

export default Mission_03;