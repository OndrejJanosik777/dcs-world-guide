import React, { Component } from 'react';
import { useState } from 'react';
// pages
import AntiIce from './anti-ice/anti-ice';
import B8V20Rockets from './b8v20-rockets/b8v20-rockets';
import EngineShutdown from './engine-shutdown/engine-shutdown';
import ExteriorLights from './exterior-lights/exterior-lights';
import InteriorLights from './interior-lights/interior-lights';
import StartupProcedure from './startup_procedure/startup-procedure';
import UPK_23_250 from './upk-23-250/upk-23-250';
import MG_12_7_GL_30 from './mg-12.7-gl-30mm/mg-12.7-gl-30mm';
import Bombs from './bombs/bombs';
import SlingOP from './sling-op/sling-op';
import Ark_9 from './ark-9/ark-9';
import Ark_ud from './ark-ud/ark-ud';
import DopplerNavigation from './doppler-navigation-set/doppler-navigation-set';
import Countermeasures from './countermeasures/countermeasures';
import MiddleOverheadSection from './cockpit-layout/middle-overhead/middle-overhead';
import SideOverheadSection from './cockpit-layout/side-overhead/side-overhead';
import FrontCentral from './cockpit-layout/front-central/front-central';
import AdditionalPanels from './cockpit-layout/additional-panels/additional-panels';
import Mission_01 from './campaings/crew-part-one/mission-01/mission-01';
import Mission_02 from './campaings/crew-part-one/mission-02/mission-02';
import Mission_03 from './campaings/crew-part-one/mission-03/mission-03';

import './Mi8.scss';
import { useDispatch } from 'react-redux';
// pictures
import mi8_cover_page from './pictures/mi8_cover_page.png';


const Mi8 = () => {
    const [category, setCategory] = useState('');
    // main menues
    const [showEngineMenu, setShowEngineMenu] = useState(false);
    const [showSystemsMenu, setShowSystemsMenu] = useState(false);
    const [showCampaingMenu, setShowCampaingMenu] = useState(false);
    const [showCockpitMenu, setShowCockpitMenu] = useState(false);
    // submenues
    const [showLightsSubMenu, setShowLightsSubMenu] = useState(false);
    const [showNavigationSubMenu, setShowNavigationSubMenu] = useState(false);
    const [showWeaponsSubMenu, setShowWeaponsSubMenu] = useState(false);
    const [showCrewPartOneSubMenu, setShowCrewPartOneSubMenu] = useState(false);


    const hideAllMenu = () => {
        setShowEngineMenu(false);
        setShowSystemsMenu(false);
        setShowCampaingMenu(false);
        setShowCockpitMenu(false);
    }

    const hideAllSubMenu = () => {
        setShowLightsSubMenu(false);
        setShowNavigationSubMenu(false);
        setShowWeaponsSubMenu(false);
    }

    return (<div className='Mi8'>
        <header className='header'>
            <div className='aircraft-name'>DCS Mi-8MTV2 "Magnificant Eight"</div>
            <nav className='navbar'>
                <div className='navbar-fixed'>
                    <input type='button' className='button' value='Engine' onClick={() => { hideAllMenu(); setShowEngineMenu(!showEngineMenu) }} />
                    {/* <input type='button' className='button' value='Shutdown' onClick={() => setCategory('Engine Shutdown')} /> */}
                    {/* <input type='button' className='button' value='Weapons' onClick={() => setCategory('Weapon systems')} /> */}
                    {/* <input type='button' className='button' value='Navigation' onClick={() => setCategory('Navigation')} /> */}
                    <input type='button' className='button' value='Systems' onClick={() => { hideAllMenu(); setShowSystemsMenu(!showSystemsMenu) }} />
                    <input type='button' className='button' value='Cockpit' onClick={() => { hideAllMenu(); setShowCockpitMenu(!showCockpitMenu) }} />
                    <input type='button' className='button' value='Campaigns' onClick={() => { hideAllMenu(); setShowCampaingMenu(!showCockpitMenu) }} />
                </div>
                {showCampaingMenu ?
                    <div className='navbar-hiddable'>
                        <input type='button' value='Crew: Part One' className='submenu-lvl1' onClick={() => { hideAllSubMenu(); setShowCrewPartOneSubMenu(!showCrewPartOneSubMenu); }} />
                        {showCrewPartOneSubMenu ?
                            <div className='navbar-hiddable-submenu'>
                                <input type='button' value='> Mission 01 : NEVER FORGET' className='submenu-lvl2' onClick={() => { hideAllMenu(); setCategory('Mission 01 : NEVER FORGET'); }} />
                                <input type='button' value='> Mission 02 : VIP PASSENGER' className='submenu-lvl2' onClick={() => { hideAllMenu(); setCategory('Mission 02 : VIP PASSENGER'); }} />
                                <input type='button' value='> Mission 03 : OLD FRIENDS' className='submenu-lvl2' onClick={() => { hideAllMenu(); setCategory('Mission 03 : OLD FRIENDS'); }} />
                            </div> : <div></div>}
                        <input type='button' value='Memory of a Hero Campaign' className='submenu-lvl1' onClick={() => { setCategory('Memory of a Hero Campaign'); setShowCampaingMenu(false) }} />
                        <input type='button' value='Oilfield' className='submenu-lvl1' onClick={() => { setCategory('Oilfield'); setShowCampaingMenu(false) }} />
                        <input type='button' value='The Border' className='submenu-lvl1' onClick={() => { setCategory('The Border'); setShowCampaingMenu(false) }} />
                    </div> : <div></div>
                }
                {showCockpitMenu ?
                    <div className='navbar-hiddable'>
                        <input type='button' value='Middle Overhead Section' className='submenu-lvl1' onClick={() => { setCategory('Middle Overhead Section'); setShowCockpitMenu(false) }} />
                        <input type='button' value='Side Overhead Section' className='submenu-lvl1' onClick={() => { setCategory('Side Overhead Section'); setShowCockpitMenu(false) }} />
                        <input type='button' value='Front Central Section' className='submenu-lvl1' onClick={() => { setCategory('Front Central Section'); setShowCockpitMenu(false) }} />
                        <input type='button' value='Additional Panels' className='submenu-lvl1' onClick={() => { setCategory('Additional Panels'); setShowCockpitMenu(false) }} />
                    </div> : <div></div>
                }
                {showEngineMenu ?
                    <div className='navbar-hiddable'>
                        <input type='button' value='Start' className='submenu-lvl1' onClick={() => { setCategory('Engine Start'); setShowEngineMenu(false) }} />
                        <input type='button' value='Shutdown' className='submenu-lvl1' onClick={() => { setCategory('Engine Shutdown'); setShowEngineMenu(false) }} />
                    </div> : <div></div>
                }
                {showSystemsMenu ?
                    <div className='navbar-hiddable'>
                        <input type='button' value='Anti-Ice' className='submenu-lvl1' onClick={() => { setCategory('Anti Ice System'); setShowSystemsMenu(false) }} />
                        <input type='button' value='Countermeasures' className='submenu-lvl1' onClick={() => { setCategory('Countermeasures'); setShowSystemsMenu(false) }} />
                        <input type='button' value='Lights' className='submenu-lvl1' onClick={() => { hideAllSubMenu(); setShowLightsSubMenu(!showLightsSubMenu); }} />
                        {showLightsSubMenu ?
                            <div className='navbar-hiddable-submenu'>
                                <input type='button' value='> Interior Lights' className='submenu-lvl2' onClick={() => { hideAllMenu(); setCategory('Interior Lights'); }} />
                                <input type='button' value='> Exterior Lights' className='submenu-lvl2' onClick={() => { hideAllMenu(); setCategory('Exterior Lights'); }} />
                            </div> : <div></div>}
                        <input type='button' value='Navigation' className='submenu-lvl1' onClick={() => { hideAllSubMenu(); setShowNavigationSubMenu(!showNavigationSubMenu); }} />
                        {showNavigationSubMenu ?
                            <div className='navbar-hiddable-submenu'>
                                <input type='button' value='> ARK-9   : NDB' className='submenu-lvl2' onClick={() => { hideAllMenu(); setCategory('ARK-9'); }} />
                                <input type='button' value='> ARK-UD  : Homing set' className='submenu-lvl2' onClick={() => { hideAllMenu(); setCategory('ARK-UD'); }} />
                                <input type='button' value='> DISS-15 : Doppler Navigation Set' className='submenu-lvl2' onClick={() => { hideAllMenu(); setCategory('DISS-15'); }} />
                            </div> : <div></div>}
                        <input type='button' value='Sling Operations' className='submenu-lvl1' onClick={() => { setCategory('Sling Operations'); setShowSystemsMenu(false) }} />
                        <input type='button' value='Weapons' className='submenu-lvl1' onClick={() => { hideAllSubMenu(); setShowWeaponsSubMenu(!showWeaponsSubMenu); }} />
                        {showWeaponsSubMenu ?
                            <div className='navbar-hiddable-submenu'>
                                <input type='button' value='> B8v20 Rockets' className='submenu-lvl2' onClick={() => { hideAllMenu(); setCategory('B8v20 Rockets'); }} />
                                <input type='button' value='> UPK-23-250 Gun Containers' className='submenu-lvl2' onClick={() => { hideAllMenu(); setCategory('UPK-23-250'); }} />
                                <input type='button' value='> MG-12.7 & GL-30mm' className='submenu-lvl2' onClick={() => { hideAllMenu(); setCategory('MG-12.7-GL-30'); }} />
                                <input type='button' value='> Bombs' className='submenu-lvl2' onClick={() => { hideAllMenu(); setCategory('Bombs'); }} />
                            </div> : <div></div>}
                    </div> : <div></div>
                }
            </nav>
            <div className='category'>{category}</div>
        </header>
        <main className='main'>
            {category == 'Engine Start' ? <StartupProcedure /> : <div></div>}
            {category == 'Engine Shutdown' ? <EngineShutdown /> : <div></div>}
            {category == 'Interior Lights' ? <InteriorLights /> : <div></div>}
            {category == 'Exterior Lights' ? <ExteriorLights /> : <div></div>}
            {category == 'Anti Ice System' ? <AntiIce /> : <div></div>}
            {category == 'B8v20 Rockets' ? <B8V20Rockets /> : <div></div>}
            {category == 'UPK-23-250' ? <UPK_23_250 /> : <div></div>}
            {category == 'MG-12.7-GL-30' ? <MG_12_7_GL_30 /> : <div></div>}
            {category == 'Bombs' ? <Bombs /> : <div></div>}
            {category == 'Sling Operations' ? <SlingOP /> : <div></div>}
            {category == 'ARK-9' ? <Ark_9 /> : <div></div>}
            {category == 'ARK-UD' ? <Ark_ud /> : <div></div>}
            {category == 'DISS-15' ? <DopplerNavigation /> : <div></div>}
            {category == 'Countermeasures' ? <Countermeasures /> : <div></div>}
            {category == 'Mission 01 : NEVER FORGET' ? <Mission_01 /> : <div></div>}
            {category == 'Mission 02 : VIP PASSENGER' ? <Mission_02 /> : <div></div>}
            {category == 'Mission 03 : OLD FRIENDS' ? <Mission_03 /> : <div></div>}
            {category == 'Middle Overhead Section' ? <MiddleOverheadSection /> : <div></div>}
            {category == 'Side Overhead Section' ? <SideOverheadSection /> : <div></div>}
            {category == 'Front Central Section' ? <FrontCentral /> : <div></div>}
            {category == 'Additional Panels' ? <AdditionalPanels /> : <div></div>}
            {category == '' ? <img className='img-mi8' src={mi8_cover_page} alt='' /> : <div></div>}
        </main>
    </div>);
}

export default Mi8;