import React, { Component } from 'react';
import '../Mi8-subpage.scss';
import img_circuits_lighting from './pictures/img_circuits_lighting.png';
import img_external_lights_overview from './pictures/img_external_lights_overview.png';
import img_external_lights from './pictures/img_external_lights.png';
import img_light_beam_control from './pictures/img_light_beam_control.png';
import img_search_landing_lights from './pictures/img_search_landing_lights.png';


const ExteriorLights = () => {
    return (<div className='Mi8-subpage'>
        <ul className='fast-instructions'>
            <li>Switching on - external lights
                <ol>
                    <li>Turn on anticollision lights</li>
                    <li>Turn on formation lights</li>
                    <li>Turn on blade tip lights lights</li>
                    <li>Turn on navigation lights</li>
                    <li>Turn on taxi, langing lights if needed</li>
                </ol>
            </li>
        </ul>
        <ul className='instructions'>
            <li className='instruction'>Turning on external lights</li>
            <ol className='sub-instructions'>
                <li className='sub-instruction'>
                    <div className='desc'>Enable Circuit Breakers for external lights lights</div>
                    <div className='container'>
                        <div className='left'>UP
                            <ol>
                                <li>Left search/langing light, light control circuit</li>
                                <li>Left search/langing light, incandescent light bulb circuit</li>
                                <li>Right search/langing light, light control circuit</li>
                                <li>Right search/langing light, incandescent light bulb circuit</li>
                                <li>Navigation lights supply circuit</li>
                                <li>Formation lights supply circuit</li>
                                <li>Flash circuit</li>
                                <li>PRF-4 lights (not implemented)</li>
                            </ol>
                        </div>
                        <div className='right'>
                            <img src={img_circuits_lighting} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Search and Landing lights</div>
                    <div className='container'>
                        <div className='left'>
                            <ol>
                                <li>Left Pilot‘s FR-100 taxi light switch</li>
                                <li>Left Pilot‘s FPP-7M search/landing light control switch</li>
                                <li>Right Pilot‘s FPP-7M search/landing light control switch</li>
                            </ol>
                        </div>
                        <div className='right'>
                            <img src={img_search_landing_lights} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Controling of light beam direction</div>
                    <div className='container'>
                        <div className='left'>
                            move joypad
                        </div>
                        <div className='right'>
                            <img src={img_light_beam_control} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Another external light</div>
                    <div className='container'>
                        <div className='left'>
                            <ol>
                                <li>Anti-collision lights</li>
                                <li>Formation lights</li>
                                <li>Blade tip lights</li>
                                <li>Navigation lights</li>
                            </ol>
                        </div>
                        <div className='right'>
                            <img src={img_external_lights} alt='' />
                        </div>
                    </div>
                </li>
            </ol>
            <li className='instruction'>External lights overview</li>
            <ol className='sub-instructions'>
                <li className='sub-instruction'>
                    <div className='desc'>Enable Circuit Breakers for external lights lights</div>
                    <div className='container'>
                        <div className='left'>UP
                            <ol>
                                <li>KhS-39 Tail light</li>
                                <li>Left BANO-45 navigation light (red)</li>
                                <li>MSL-3 anti-collision light</li>
                                <li>Right pilot's FPP-7M light</li>
                                <li>FR-100 taxi light</li>
                                <li>OPS-57 formation lights</li>
                                <li>Right BANO-45 navgation light (green)</li>
                                <li>Blade Tip lights</li>
                            </ol>
                        </div>
                        <div className='right'>
                            <img src={img_external_lights_overview} alt='' />
                        </div>
                    </div>
                </li>
            </ol>
        </ul>
    </div>);
}

export default ExteriorLights;