import React, { Component } from 'react';
import '../Mi8-subpage.scss';
import img_digital_display_unit from './pictures/img_digital_display_unit.png';
import img_doppler_control_panel from './pictures/img_doppler_control_panel.png';
import img_doppler_selector_switch from './pictures/img_doppler_selector_switch.png';
import img_doppler_switch from './pictures/img_doppler_switch.png';
import img_ground_speed_drift_indicator from './pictures/img_ground_speed_drift_indicator.png';
import img_stationary_flight_indicator from './pictures/img_stationary_flight_indicator.png';

const DopplerNavigation = () => {
    return (<div className='Mi8-subpage'>
        <ul className='fast-instructions'>
            <li>Doppler operation
                <ol>
                    <li>Turn on circuit breakers</li>
                    <li>Dopler selector - MEMORY</li>
                    <li>Doppler Switch - UP</li>
                    <li>Set Digital Display</li>
                </ol>
            </li>
        </ul>
        <ul className='instructions'>
            <li className='instruction'>Doppler operation</li>
            <ol className='sub-instructions'>
                <li className='sub-instruction'>
                    <div className='desc'>Circuit breakers - Up</div>
                    <div className='container'>
                        <div className='left'>
                            <strong>--</strong>
                        </div>
                        <div className='right'>
                            <strong>--</strong>
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Dopler selector</div>
                    <div className='container'>
                        <div className='left'>
                            <div>Set to MEMORY</div>
                            <div>b) ICS-RADIO Selector to DOWN</div>
                        </div>
                        <div className='right'>
                            <img src={img_doppler_selector_switch} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Doppler Switch - UP</div>
                    <div className='container'>
                        <div className='left'>
                            <div>Set Switch - UP</div>
                        </div>
                        <div className='right'>
                            <img src={img_doppler_switch} alt='' />
                        </div>
                    </div>
                </li>
            </ol>
            <li className='instruction'>Doppler Panels</li>
            <ol className='sub-instructions'>
                <li className='sub-instruction'>
                    <div className='desc'>Digital Display Unit - overview</div>
                    <div className='container'>
                        <div className='left'>
                            <div>1) LATERAL DEVIATION counter; (LEFT, RIGHT)</div>
                            <div>2) LEFT; RIGHT BUTTONS to reset lateral deviation</div>
                            <div>3) DISTANCE COUNTER;</div>
                            <div>4) AFT, FWD buttons </div>
                            <div>5) COURSE ANGLE counter</div>
                            <div>6) „+“ and „-“ buttons</div>
                            <div>7) ON LIGHT - indicates operation of unit</div>
                            <div>8) ON; OFF buttons engage/disengage digital readout</div>
                        </div>
                        <div className='right'>
                            <img src={img_digital_display_unit} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Stationary Flight Indicator - overview</div>
                    <div className='container'>
                        <div className='left'>
                            <div>1) Vertical Pointer</div>
                            <div>2) Lateral and Longitudinal indexes</div>
                            <div>3) OFF LIGHT</div>
                        </div>
                        <div className='right'>
                            <img src={img_stationary_flight_indicator} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Ground Speed Drift Indicator - overview</div>
                    <div className='container'>
                        <div className='left'>
                            <div>1) GROUND SPEED WINDOW in km/h</div>
                            <div>2) DRIFT INDICATOR NEEDLE</div>
                            <div>3) LIGHT - MEMORY MODE</div>
                            <div>4) TEST - OPERATE switch</div>
                            <div>5) LAND -SEA switch</div>
                        </div>
                        <div className='right'>
                            <img src={img_ground_speed_drift_indicator} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Doppler Control Panel - overview</div>
                    <div className='container'>
                        <div className='left'>
                            <div>1) MODE SELECTOR; 1-4 perform test; 5-operate</div>
                            <div>2) OPERATE LIGHT</div>
                            <div>3) „B“ LIGHT - Iluminates if the Doppler computer fails</div>
                            <div>4) TEST LIGHT - indicates that system is in test mode</div>
                            <div>5) M LIGHT - magneton failure</div>
                        </div>
                        <div className='right'>
                            <img src={img_doppler_control_panel} alt='' />
                        </div>
                    </div>
                </li>
            </ol>
        </ul>
    </div>);
}

export default DopplerNavigation;