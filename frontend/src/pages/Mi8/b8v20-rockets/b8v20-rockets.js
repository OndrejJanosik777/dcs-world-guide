import React, { Component } from 'react';
import '../Mi8-subpage.scss';
import img_copilots_weapon_control_panel_1 from './pictures/img_copilots_weapon_control_panel_1.png';
import img_fire_button from './pictures/img_fire_button.png';
import img_gun_camera from './pictures/img_gun_camera.png';
import img_master_arm_switch from './pictures/img_master_arm_switch.png';
import img_pilots_lower_arm_panel_1 from './pictures/img_pilots_lower_arm_panel_1.png';
import img_pilots_upper_arm_panel_1 from './pictures/img_pilots_upper_arm_panel_1.png';
import img_weapons_cb from './pictures/img_weapons_cb.png';

const B8V20Rockets = () => {
    return (<div className='Mi8-subpage'>
        <ul className='fast-instructions'>
            <li>Engaging B8v20 rockets - overview
                <ol>
                    <li>Circuit breakers</li>
                    <li>Master Arm Switch</li>
                    <li>Pilot's upper arm panel</li>
                    <li>Pilot's lower arm panel</li>
                    <li>Gun Camera - optional</li>
                    <li>Copilot's weapon control panel</li>
                    <li>Fire</li>
                </ol>
            </li>
        </ul>
        <ul className='instructions'>
            <li className='instruction'>Deploying B8v20 rockets</li>
            <ol className='sub-instructions'>
                <li className='sub-instruction'>
                    <div className='desc'>Circuit Brakers - UP</div>
                    <div className='container'>
                        <div className='left'>
                            <strong>--</strong>
                        </div>
                        <div className='right'>
                            <img src={img_weapons_cb} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Enable master arm switch</div>
                    <div className='container'>
                        <div className='left'>
                            <strong>--</strong>
                        </div>
                        <div className='right'>
                            <img src={img_master_arm_switch} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Pilot's Upper Armament Panel</div>
                    <div className='container'>
                        <div className='left'>
                            <div>a) PRESS check lamps button</div>
                            <div>b) CHECK LIGHTS - that all lamps are on</div>
                            <div>c) SWITCH UP - RKT Gun maser switch</div>
                            <div>d) CHECK LIGHTS - hardpoints 3, 4 (1,2,5,6) are loaded</div>
                            <div>e) CHECK LIGHTS - rockets launching circuit</div>
                            <div>f) CHECK LIGHTS - launchers 3,4 are on</div>
                            <div>3) PRESS IF - launchers are off, ARM FCU LAUNCHERS button, set the launchers to their initial position</div>
                        </div>
                        <div className='right'>
                            <img src={img_pilots_upper_arm_panel_1} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Pilot's Lower Armament Panel</div>
                    <div className='container'>
                        <div className='left'>
                            <div>a) SET - Burst 8 or 16  or 4 rockets quantity per launcher</div>
                            <div>b) SELECT -  required launchers 1-2-5-6 or AUTO or 3-4</div>
                            <div>c) SWITCH DOWN - connect rocket launching circuits to the combat button</div>
                        </div>
                        <div className='right'>
                            <img src={img_pilots_lower_arm_panel_1} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Gun Camera (optionally)</div>
                    <div className='container'>
                        <div className='left'>
                            (optionally)
                            Enable gun camera, player must enable gun camera functionality
                        </div>
                        <div className='right'>
                            <img src={img_gun_camera} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>
                        Copilot's Weapons Control Panel
                    </div>
                    <div className='container'>
                        <div className='left'>
                            On co-pilots weapon control panel set the Payload profile selector to the position, corresponding to the current payload: shooting is possible from only those hardpoints, which have the symbol in the payload profile
                        </div>
                        <div className='right'>
                            <img src={img_copilots_weapon_control_panel_1} alt='' />
                        </div>
                    </div>
                </li>
                <li className='sub-instruction'>
                    <div className='desc'>Fire</div>
                    <div className='container'>
                        <div className='left'>
                            Press fire button
                        </div>
                        <div className='right'>
                            <img src={img_fire_button} alt='' />
                        </div>
                    </div>
                </li>
            </ol>

        </ul>
    </div>);
}

export default B8V20Rockets;